package tripleo.elijah.comp;

import java.util.function.Consumer;

public interface GonResponseChannel {
	<T> T read(Class<? extends T> aServiceClass, Consumer<T> cons);

	void readError(Class<?> aO, Consumer<Throwable> cpns);

	void waitResponse();
}

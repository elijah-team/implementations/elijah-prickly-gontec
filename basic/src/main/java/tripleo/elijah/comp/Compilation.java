package tripleo.elijah.comp;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.lang.OS_Package;
import tripleo.elijah.lang.Qualident;
import tripleo.elijah.nextgen.outputtree.EOT_OutputTree;
import tripleo.elijah.nextgen.query.Operation2;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.deduce.FunctionMapHook;
import tripleo.elijah.stages.deduce.fluffy.i.FluffyComp;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.util.NotImplementedException;

import java.io.File;
import java.util.List;

public interface Compilation {
	static ElLog.Verbosity gitlabCIVerbosity() {
		final boolean gitlab_ci = isGitlab_ci();
		return gitlab_ci ? ElLog.Verbosity.SILENT : ElLog.Verbosity.VERBOSE;
	}

	static boolean isGitlab_ci() {
		return System.getenv("GITLAB_CI") != null;
	}

	default CompilationEnclosure getCompilationEnclosure() {
		throw new NotImplementedException();
	}

	void hasInstructions(@NotNull List<CompilerInstructions> cis) throws Exception;

	void feedCmdLine(@NotNull List<String> args);

	void feedCmdLine(List<String> args, CompilerController ctl);

	String getProjectName();

	OS_Module realParseElijjahFile(String f, @NotNull File file, boolean do_out) throws Exception;

	void pushItem(CompilerInstructions aci);

	List<ClassStatement> findClass(String string);

	void use(@NotNull CompilerInstructions compilerInstructions, boolean do_out) throws Exception;

	int errorCount();

	void writeLogs(boolean aSilent, @NotNull List<ElLog> aLogs);

	ErrSink getErrSink();

	IO getIO();

	void addModule(OS_Module module, String fn);

	OS_Module fileNameToModule(String fileName);

	boolean getSilence();

	Operation2<OS_Module> findPrelude(String prelude_name);

	void addFunctionMapHook(FunctionMapHook aFunctionMapHook);

	@NotNull DeducePhase getDeducePhase();

	int nextClassCode();

	int nextFunctionCode();

	OS_Package getPackage(Qualident pkg_name);

	OS_Package makePackage(Qualident pkg_name);

	int compilationNumber();

	String getCompilationNumberString();

	@Deprecated
	int modules_size();

	@NotNull EOT_OutputTree getOutputTree();

	@NotNull FluffyComp getFluffy();

	@NotNull List<GeneratedNode> getLGC();

	boolean isPackage(String aPackageName);

	Pipeline getPipelines();

	ModuleBuilder moduleBuilder();

	Finally reports();

	List<ElLog> _accessElLogs();

	boolean cfg_silent();
}

package tripleo.elijah_gontec.service;

import tripleo.elijah.comp.GonResponseChannel;

import java.io.InputStream;

public interface GonCompilationService extends IGonService {
	GonResponseChannel submit(String aF, InputStream aS);
}

Elijah prolific remnant
========================

Elijah is:

- ... a high-level language suitable for replacement of* Java and C/C++.
- ... a historical curiosity.
- ... is meant to integrate into current C and Java projects. 
- ... is free software intended for use on all systems, including GNU/Linux.
- ... is licensed under LGPL.

`prolific-remnant` is:

- ... implemented in Java (17)
- ... uses Maven

Instructions
-------------

[https://github.com/elijah-team/prolific-remnant](https://github.com/elijah-team/prolific-remnant)

```shell
git clone https://github.com/elijah-team/prolific-remnant -b pr-fluffy-gontec
mkdir prolific-remnant/COMP
( cd prolific-remnant && nix-shell -p maven jdk17 --pure --command "mvn test")
```

Goals
------

- Violate the sanctity of the fake multi pom with a real multi pom

- Move `mal` up the food chain

- Promote the interface model (comp, lang) + (ie dt, gdm, g??) + (maybe ck/cm, eit/eot)

- Being that this is a derivative of `mainline-k`, try adding `cef` stuff

- Make way for Rosetta type things

Lineage
--------

- `Fluffy mainline-k`

- `Fluffy mainline-k-2023-09`

- `cogent` somewhere, whatever that is

- other tings since the grand rejigger

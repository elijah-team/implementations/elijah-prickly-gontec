package tripleo.elijah;

import org.junit.*;
import tripleo.elijah.lang.*;
import tripleo.elijah.util.*;

public class QualidentToDotExpressionTest {

	@Test
	public void qualidentToDotExpression2() {
		final Qualident q = new Qualident();
		q.append(Helpers0.string_to_ident("a"));
		q.append(Helpers0.string_to_ident("b"));
		q.append(Helpers0.string_to_ident("c"));
		final IExpression e = Helpers0.qualidentToDotExpression2(q);
		System.out.println(e);
		Assert.assertEquals("a.b.c", e.toString());
	}
}
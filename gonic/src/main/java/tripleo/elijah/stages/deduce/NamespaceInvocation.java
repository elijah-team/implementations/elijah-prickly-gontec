/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce;

import org.jdeferred2.*;
import org.jdeferred2.impl.*;
import org.jetbrains.annotations.*;
import tripleo.elijah.lang.*;
import tripleo.elijah.stages.gen_fn.*;

/**
 * Created 5/31/21 12:00 PM
 */
public class NamespaceInvocation implements INamespaceInvocation {

	private final DeferredObject<IGeneratedNamespace, Void, Void> resolveDeferred = new DeferredObject<IGeneratedNamespace, Void, Void>();
	private final NamespaceStatement                             namespaceStatement;

	public NamespaceInvocation(final NamespaceStatement aNamespaceStatement) {
		namespaceStatement = aNamespaceStatement;
	}

	@Override
	public @NotNull DeferredObject<IGeneratedNamespace, Void, Void> resolveDeferred() {
		return resolveDeferred;
	}

	@Override
	public @NotNull Promise<IGeneratedNamespace, Void, Void> resolvePromise() {
		return resolveDeferred.promise();
	}

	@Override
	public NamespaceStatement getNamespace() {
		return namespaceStatement;
	}

	@Override
	public void setForFunctionInvocation(@NotNull final IFunctionInvocation aFunctionInvocation) {
		aFunctionInvocation.setNamespaceInvocation(this);
	}
}

//
//
//

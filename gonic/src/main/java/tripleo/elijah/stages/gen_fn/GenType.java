/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn;

import org.jdeferred2.*;
import org.jetbrains.annotations.*;
import tripleo.elijah.comp.*;
import tripleo.elijah.lang.*;
import tripleo.elijah.lang.types.*;
import tripleo.elijah.stages.deduce.*;
import tripleo.elijah.stages.deduce.zero.*;
import tripleo.elijah.util.*;

import java.util.*;

/**
 * Created 5/31/21 1:32 PM
 */
public class GenType implements IGenType {
	public NamespaceStatement resolvedn;
	public OS_Type            typeName; // TODO or just TypeName ??
	public TypeName           nonGenericTypeName;
	public OS_Type            resolved;
	public IInvocation        ci;
	public GeneratedNode      node;
	public FunctionInvocation functionInvocation;

	@Contract(pure = true)
	public GenType(final NamespaceStatement aNamespaceStatement) {
		resolvedn = /*new OS_Type*/(aNamespaceStatement);
	}

	public GenType(final @NotNull ClassStatement aClassStatement) {
		resolved = aClassStatement.getOS_Type();
	}

	public GenType(final OS_Type aAttached,
	               final OS_Type aOS_type,
	               final boolean aB,
	               final TypeName aTypeName,
	               final DeduceTypes2 deduceTypes2,
	               final ErrSink errSink,
	               final DeducePhase phase) {
		typeName = aAttached;
		resolved = aOS_type;
		if (aB) {
			ci = genCI(aTypeName, deduceTypes2, errSink, phase);
		}
	}

	@Override
	public IClassInvocation genCI(final TypeName aGenericTypeName,
								  final DeduceTypes2 deduceTypes2,
								  final ErrSink errSink,
								  final DeducePhase phase) {
		final SetGenCI        sgci = new SetGenCI();
		final IClassInvocation ci   = sgci.call(this, aGenericTypeName, deduceTypes2, errSink, phase);
		return ci;
	}

	public GenType() {
	}

	@Override
	public boolean equals(final Object aO) {
		if (this == aO) return true;
		if (aO == null || getClass() != aO.getClass()) return false;

		final GenType genType = (GenType) aO;

		if (!Objects.equals(resolvedn, genType.resolvedn)) return false;
		if (!Objects.equals(typeName, genType.typeName)) return false;
		if (!Objects.equals(nonGenericTypeName, genType.nonGenericTypeName))
			return false;
		if (!Objects.equals(resolved, genType.resolved)) return false;
		if (!Objects.equals(ci, genType.ci)) return false;
		if (!Objects.equals(node, genType.node)) return false;
		return Objects.equals(functionInvocation, genType.functionInvocation);
	}

	@Override
	public int hashCode() {
		int result = resolvedn != null ? resolvedn.hashCode() : 0;
		result = 31 * result + (typeName != null ? typeName.hashCode() : 0);
		result = 31 * result + (nonGenericTypeName != null ? nonGenericTypeName.hashCode() : 0);
		result = 31 * result + (resolved != null ? resolved.hashCode() : 0);
		result = 31 * result + (ci != null ? ci.hashCode() : 0);
		result = 31 * result + (node != null ? node.hashCode() : 0);
		result = 31 * result + (functionInvocation != null ? functionInvocation.hashCode() : 0);
		return result;
	}

	@Override
	public String asString() {
		final String sb = "GenType{" + "resolvedn=" + resolvedn +
		  ", typeName=" + typeName +
		  ", nonGenericTypeName=" + nonGenericTypeName +
		  ", resolved=" + resolved +
		  ", ci=" + ci +
		  ", node=" + node +
		  ", functionInvocation=" + functionInvocation +
		  '}';
		return sb;
	}

	@Override
	public void set(final @NotNull OS_Type aType) {
		switch (aType.getType()) {
		case USER:
			typeName = aType;
			break;
		case USER_CLASS:
			resolved = aType;
			break;
		default:
			SimplePrintLoggerToRemoveSoon.println_err2("48 Unknown in set: " + aType);
		}
	}

	@Override
	public boolean isNull() {
		if (resolvedn != null) return false;
		if (typeName != null) return false;
		if (nonGenericTypeName != null) return false;
		if (resolved != null) return false;
		if (ci != null) return false;
		return node == null;
	}

	@Override
	public void copy(final GenType aGenType) {
		if (resolvedn == null) resolvedn = aGenType.resolvedn;
		if (typeName == null) typeName = aGenType.typeName;
		if (nonGenericTypeName == null) nonGenericTypeName = aGenType.nonGenericTypeName;
		if (resolved == null) resolved = aGenType.resolved;
		if (ci == null) ci = aGenType.ci;
		if (node == null) node = aGenType.node;
	}

	@Override
	public void genCIForGenType2(final DeduceTypes2 aDeduceTypes2) {
		genCI(nonGenericTypeName, aDeduceTypes2, aDeduceTypes2._errSink(), aDeduceTypes2._phase());
		final IInvocation invocation = ci;
		if (invocation instanceof final INamespaceInvocation namespaceInvocation) {
			namespaceInvocation.resolveDeferred().then(new DoneCallback<IGeneratedNamespace>() {
				@Override
				public void onDone(final GeneratedNamespace result) {
					node = result;
				}
			});
		} else if (invocation instanceof final IClassInvocation classInvocation) {
			classInvocation.resolvePromise().then(new DoneCallback<IGeneratedClass>() {
				@Override
				public void onDone(final GeneratedClass result) {
					node = result;
				}
			});
		} else {
			if (resolved instanceof final OS_FuncExprType funcExprType) {

				final Zero_FuncExprType zfet = aDeduceTypes2.getZero(funcExprType);

				node = zfet.genCIForGenType2(aDeduceTypes2);
			} else if (resolved instanceof final OS_FuncType funcType) {
				final int         y        = 2;
			} else
				throw new IllegalStateException("invalid invocation");
		}
	}

	/**
	 * Sets the node for a GenType, invocation must already be set
	 */
	@Override
	public void genNodeForGenType2() {
//		assert aGenType.nonGenericTypeName != null;

		final IInvocation invocation = ci;

		if (invocation instanceof final INamespaceInvocation namespaceInvocation) {
			namespaceInvocation.resolveDeferred().then(new DoneCallback<IGeneratedNamespace>() {
				@Override
				public void onDone(final GeneratedNamespace result) {
					node = result;
				}
			});
		} else if (invocation instanceof final IClassInvocation classInvocation) {
			classInvocation.resolvePromise().then(new DoneCallback<IGeneratedClass>() {
				@Override
				public void onDone(final GeneratedClass result) {
					node = result;
				}
			});
		} else
			throw new IllegalStateException("invalid invocation");
	}

}

//
//
//

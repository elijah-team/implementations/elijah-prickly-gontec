/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn;

import org.jdeferred2.DoneCallback;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.Eventual;
import tripleo.elijah.comp.ErrSink;
import tripleo.elijah.lang.Context;
import tripleo.elijah.lang.IExpression;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.stages.deduce.*;
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry;
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3;
import tripleo.elijah.stages.deduce.zero.IPTE_Zero;
import tripleo.elijah.stages.deduce.zero.PTE_Zero;
import tripleo.elijah.stages.instructions.InstructionArgument;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.util.Helpers0;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;
import tripleo.elijah_prolific.deduce.DT_Element3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created 9/12/20 10:07 PM
 */
public class ProcTableEntry extends BaseTableEntry implements IProcTableEntry {
	public final  int                                             index;
	public final  List<ITypeTableEntry>                            args;
	/**
	 * Either a hint to the programmer-- The compiler should be able to work without this.
	 * <br/>
	 * Or for synthetic methods
	 */
	public final  IExpression                                     expression;
	public final  InstructionArgument                             expression_num;
	public final  DeduceProcCall                                  dpc                   = new DeduceProcCall(this);
	private final DeferredObject<IProcTableEntry, Void, Void>      completeDeferred      = new DeferredObject<>();
	private final DeferredObject2<IFunctionInvocation, Void, Void> onFunctionInvocations = new DeferredObject2<>();
	private final DeferredObject<IGenType, Void, Void>             typeDeferred          = new DeferredObject<>();
	private final Eventual<DT_Element3>                           _p_resolvedElement    = new Eventual<>();
	private       ClassInvocation                                 classInvocation;
	private       FunctionInvocation                              functionInvocation;
	private       DeduceElement3_ProcTableEntry                   _de3;
	private       PTE_Zero                                        _zero;

	public ProcTableEntry(final int aIndex, final IExpression aExpression, final InstructionArgument aExpressionNum, final List<ITypeTableEntry> aArgs) {
		index          = aIndex;
		expression     = aExpression;
		expression_num = aExpressionNum;
		args           = aArgs;

		addStatusListener(new StatusListener() {
			@Override
			public void onChange(final IElementHolder eh, final Status newStatus) {
				if (newStatus == Status.KNOWN) {
					setResolvedElement(eh.getElement());
				}
			}
		});

		for (final ITypeTableEntry tte : args) {
			tte.addSetAttached(new ITypeTableEntry.OnSetAttached() {
				@Override
				public void onSetAttached(final ITypeTableEntry aTypeTableEntry) {
					ProcTableEntry.this.onSetAttached();
				}
			});
		}

		elementPromise(
				p -> _p_resolvedElement.resolve(new __DT_Element3(p)),
				_p_resolvedElement::fail);

		setupResolve();
	}

	@Override
	public void onSetAttached() {
		int state = 0;
		if (args != null) {
			final int ac  = args.size();
			int       acx = 0;
			for (final ITypeTableEntry tte : args) {
				if (tte.getAttached() != null)
					acx++;
			}
			if (acx < ac) {
				state = 1;
			} else if (acx > ac) {
				state = 2;
			} else if (acx == ac) {
				state = 3;
			}
		} else {
			state = 3;
		}
		switch (state) {
			case 0:
				throw new IllegalStateException();
			case 1:
				SimplePrintLoggerToRemoveSoon.println_err2("136 pte not finished resolving " + this);
				break;
			case 2:
				SimplePrintLoggerToRemoveSoon.println_err2("138 Internal compiler error");
				break;
			case 3:
				if (completeDeferred.isPending())
					completeDeferred.resolve(this);
				break;
			default:
				throw new NotImplementedException();
		}
	}

	@Override
	public void setArgType(final int aIndex, final OS_Type aType) {
		args.get(aIndex).setAttached(aType);
	}

	@Override
	public ClassInvocation getClassInvocation() {
		return classInvocation;
	}

	@Override
	public void setClassInvocation(final ClassInvocation aClassInvocation) {
		classInvocation = aClassInvocation;
	}

	@Override
	@NotNull
	public String toString() {
		return "ProcTableEntry{" +
				"index=" + index +
				", expression=" + expression +
				", expression_num=" + expression_num +
				", args=" + args +
				'}';
	}

	// have no idea what this is for
	@Override
	public void onFunctionInvocation(final DoneCallback<IFunctionInvocation> callback) {
		onFunctionInvocations.then(callback);
	}

	@Override
	public DeferredObject<IGenType, Void, Void> typeDeferred() {
		return typeDeferred;
	}

	@Override
	public Promise<IGenType, Void, Void> typePromise() {
		return typeDeferred.promise();
	}

	@Override
	public FunctionInvocation getFunctionInvocation() {
		return functionInvocation;
	}

	// have no idea what this is for
	@Override
	public void setFunctionInvocation(final FunctionInvocation aFunctionInvocation) {
		if (functionInvocation != null && functionInvocation.sameAs(aFunctionInvocation))
			return; // short circuit for better behavior
		//ABOVE 2b
		if (functionInvocation != aFunctionInvocation) {
			functionInvocation = aFunctionInvocation;
			onFunctionInvocations.reset();
			onFunctionInvocations.resolve(functionInvocation);
		}
	}

	@Override
	public DeduceProcCall deduceProcCall() {
		return dpc;
	}

	@Override
	@NotNull
	public String getLoggingString(final @Nullable DeduceTypes2 aDeduceTypes2, final ElLog LOG) {
		final String                pte_string;
		@NotNull final List<String> l = new ArrayList<String>();

		for (@NotNull final TypeTableEntry typeTableEntry : getArgs()) {
			final OS_Type attached = typeTableEntry.getAttached();

			if (attached != null)
				l.add(attached.toString());
			else {
//				if (aDeduceTypes2 != null)
//					LOG.err("267 attached == null for "+typeTableEntry);

				if (typeTableEntry.expression != null)
					l.add(String.format("<Unknown expression: %s>", typeTableEntry.expression));
				else
					l.add("<Unknkown>");
			}
		}

		final String sb2 = "[" +
				Helpers0.String_join(", ", l) +
				"]";
		pte_string = sb2;
		return pte_string;
	}

	@Override
	public List<ITypeTableEntry> getArgs() {
		return args;
	}

	@Override
	public void setDeduceTypes2(final DeduceTypes2 aDeduceTypes2, final Context aContext, final BaseGeneratedFunction aGeneratedFunction, final ErrSink aErrSink) {
		_p_resolvedElement.then(xx -> {
			xx.setDeduceTypes2(aDeduceTypes2);
			xx.setContext(aContext);
			xx.setGeneratedFunction(aGeneratedFunction);
			xx.setErrSink(aErrSink);
		});
		dpc.setDeduceTypes2(aDeduceTypes2, aContext, aGeneratedFunction, aErrSink);
	}

	@Override
	public IDeduceElement3 getDeduceElement3() {
		assert dpc._deduceTypes2() != null; // TODO setDeduce... called; Promise?

		return getDeduceElement3(dpc._deduceTypes2(), dpc._generatedFunction());
	}

	@Override
	public IDeduceElement3 getDeduceElement3(final DeduceTypes2 aDeduceTypes2, final BaseGeneratedFunction aGeneratedFunction) {
		if (_de3 == null) {
			_de3 = new DeduceElement3_ProcTableEntry(this, aDeduceTypes2, aGeneratedFunction);
//			_de3.
		}
		return _de3;
	}

	@Override
	public IPTE_Zero zero() {
		if (_zero == null)
			_zero = new PTE_Zero(this);

		return _zero;
	}

	@Override
	public void onResolvedElement(final Consumer<DT_Element3> cb) {
		_p_resolvedElement.then(cb::accept);
	}

}

//
//
//

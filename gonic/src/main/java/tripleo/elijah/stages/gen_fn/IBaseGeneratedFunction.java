package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.Context;
import tripleo.elijah.stages.deduce.DeduceTypes2;
import tripleo.elijah.stages.gen_generic.IDependencyReferent;
import tripleo.elijah.stages.instructions.*;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;

import java.util.LinkedList;
import java.util.List;

public interface IBaseGeneratedFunction extends DependencyTracker, GeneratedNode, DeduceTypes2.ExpectationBase, IDependencyReferent {
	static void printTables(GeneratedFunction gf) {
		SimplePrintLoggerToRemoveSoon.println_out("VariableTable ");
		for (final VariableTableEntry variableTableEntry : gf.vte_list) {
			SimplePrintLoggerToRemoveSoon.println_out("\t" + variableTableEntry);
		}
		SimplePrintLoggerToRemoveSoon.println_out("ConstantTable ");
		for (final ConstantTableEntry constantTableEntry : gf.cte_list) {
			SimplePrintLoggerToRemoveSoon.println_out("\t" + constantTableEntry);
		}
		SimplePrintLoggerToRemoveSoon.println_out("ProcTable     ");
		for (final IProcTableEntry procTableEntry : gf.prte_list) {
			SimplePrintLoggerToRemoveSoon.println_out("\t" + procTableEntry);
		}
		SimplePrintLoggerToRemoveSoon.println_out("TypeTable     ");
		for (final ITypeTableEntry typeTableEntry : gf.tte_list) {
			SimplePrintLoggerToRemoveSoon.println_out("\t" + typeTableEntry);
		}
		SimplePrintLoggerToRemoveSoon.println_out("IdentTable    ");
		for (final IdentTableEntry identTableEntry : gf.idte_list) {
			SimplePrintLoggerToRemoveSoon.println_out("\t" + identTableEntry);
		}
	}

	static @NotNull List<InstructionArgument> _getIdentIAPathList(@NotNull InstructionArgument oo) {
		final LinkedList<InstructionArgument> s = new LinkedList<InstructionArgument>();
		while (oo != null) {
			if (oo instanceof IntegerIA) {
				s.addFirst(oo);
				oo = null;
			} else if (oo instanceof IdentIA) {
				final IdentTableEntry ite1 = ((IdentIA) oo).getEntry();
				s.addFirst(oo);
				oo = ite1.getBacklink();
			} else if (oo instanceof ProcIA) {
				s.addFirst(oo);
				oo = null;
			} else
				throw new IllegalStateException("Invalid InstructionArgument");
		}
		return s;
	}

	String getIdentIAPathNormal(IdentIA ia2);

	@NotNull VariableTableEntry getVarTableEntry(int index);

	@NotNull IdentTableEntry getIdentTableEntry(int index);

	@NotNull ProcTableEntry getProcTableEntry(int index);

	@NotNull List<Instruction> instructions();

	Instruction getInstruction(int anIndex);

	int add(InstructionName aName, List<InstructionArgument> args_, Context ctx);
}

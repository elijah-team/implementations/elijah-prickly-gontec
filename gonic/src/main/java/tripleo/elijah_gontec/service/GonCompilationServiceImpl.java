package tripleo.elijah_gontec.service;

import antlr.InputBuffer;
import antlr.RecognitionException;
import antlr.TokenStreamException;

import tripleo.elijah.Out;
import tripleo.elijah.comp.Compilation;
import tripleo.elijah.lang.OS_Module;

import tripleo.elijjah.ElijjahLexer;
import tripleo.elijjah.ElijjahParser;

public class GonCompilationServiceImpl {
    public void onSubmit() {

    }

    public void action() {
        InputBuffer s           = null;
        String      f           = null;
        Compilation compilation = null;
        boolean     do_out      = false;

        final ElijjahLexer lexer = new ElijjahLexer(s);
        lexer.setFilename(f);
        final ElijjahParser parser = new ElijjahParser(lexer);
        parser.out = new Out(f, compilation, do_out);
        parser.setFilename(f);
        try {
            parser.program();
        } catch (final RecognitionException | TokenStreamException aE) {
            return ;//Operation.failure(aE);
        }
        final OS_Module module = parser.out.module();
        parser.out = null;
    }
}

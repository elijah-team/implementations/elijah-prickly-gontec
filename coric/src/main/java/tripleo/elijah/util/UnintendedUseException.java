package tripleo.elijah.util;

public class UnintendedUseException extends RuntimeException {
    public UnintendedUseException(String s) {
        super(s);
    }

    public UnintendedUseException() {
        super();
    }
}

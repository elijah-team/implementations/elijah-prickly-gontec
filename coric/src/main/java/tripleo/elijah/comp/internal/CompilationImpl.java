package tripleo.elijah.comp.internal;

import org.jetbrains.annotations.*;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.*;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.lang.OS_Package;
import tripleo.elijah.lang.Qualident;
import tripleo.elijah.nextgen.outputtree.*;
import tripleo.elijah.nextgen.query.Operation2;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.deduce.FunctionMapHook;
import tripleo.elijah.stages.deduce.fluffy.i.*;
import tripleo.elijah.stages.deduce.fluffy.impl.*;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.testing.comp.*;
import tripleo.elijah.util.*;

import java.io.File;
import java.util.*;

public class CompilationImpl extends CompilationImpl2 implements Compilation {
	private final @NotNull FluffyCompImpl   _fluffyComp;

	public static ElLog.Verbosity gitlabCIVerbosity() {
		return Compilation.gitlabCIVerbosity();
	}

	public static boolean isGitlab_ci() {
		return Compilation.isGitlab_ci();
	}

	@Override
	public OS_Module realParseElijjahFile(String f, @NotNull File file, boolean do_out) throws Exception {
//		return null;
		throw new UnintendedUseException("??");
	}

	private @Nullable EOT_OutputTree   _output_tree = null;

	public CompilationImpl(final ErrSink aEee, final IO aIo) {
		super(aEee, aIo);
		_fluffyComp = new FluffyCompImpl(this);
	}

	public void testMapHooks(final List<IFunctionMapHook> aMapHooks) {
		throw new NotImplementedException();
	}

	@Override
	public @NotNull EOT_OutputTree getOutputTree() {
		if (_output_tree == null) {
			_output_tree = new EOT_OutputTree();
		}

		assert _output_tree != null;

		return _output_tree;
	}

	@Override
	public @NotNull FluffyComp getFluffy() {
		return _fluffyComp;
	}

	@Override
	public List<ElLog> _accessElLogs() {
		return null;
	}

	@Override
    public boolean cfg_silent() {
        return cfg.silent;
    }

    public ICompilationAccess _access() {
		return new DefaultCompilationAccess(this);
	}
}

/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.internal;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.subjects.ReplaySubject;
import io.reactivex.rxjava3.subjects.Subject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah.comp.*;
import tripleo.elijah.comp.functionality.f202.F202;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.lang.OS_Package;
import tripleo.elijah.lang.Qualident;
import tripleo.elijah.nextgen.inputtree.EIT_ModuleInput;
import tripleo.elijah.nextgen.query.Operation2;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.deduce.FunctionMapHook;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.util.Helpers;
import tripleo.elijah.util.Helpers0;
import tripleo.elijah.world.i.WorldModule;
import tripleo.elijah.world.impl.DefaultLivingRepo;
import tripleo.elijah.world.impl.DefaultWorldModule;
import tripleo.elijah_prolific.v.V;

import java.io.File;
import java.util.*;

public abstract class CompilationImpl2 implements Compilation {
	public final  List<ElLog>       elLogs = new LinkedList<>();
	public final  CompilationConfig cfg    = new CompilationConfig();
	public final  CIS               _cis   = new CIS();
	//
	public final  DefaultLivingRepo _repo  = new DefaultLivingRepo();
	final         MOD               _mod   = new MOD(this);
	private final USE               _use   = new USE(this);
	//
	private final Pipeline          pipelines;
	private final int               _compilationNumber;
	private final ErrSink           errSink;
	private final IO                io;
	private final CompFactory       _con   = new CompFactory() {
		@Override
		public @NotNull EIT_ModuleInput createModuleInput(final OS_Module aModule) {
			return new EIT_ModuleInput(aModule, null);//Compilation.this);
		}

		@Override
		public @NotNull Qualident createQualident(final @NotNull List<String> sl) {
			var R = new Qualident();
			for (String s : sl) {
				R.append(Helpers.string_to_ident(s));
			}
			return R;
		}

		@Override
		public @NotNull InputRequest createInputRequest(final File aFile, final boolean aDo_out, final @Nullable LibraryStatementPart aLsp) {
			return new InputRequest(aFile, aDo_out, aLsp);
		}

		@Override
		public @NotNull WorldModule createWorldModule(final OS_Module m) {
			CompilationEnclosure ce = getCompilationEnclosure();
			final WorldModule    R  = new DefaultWorldModule(m, ce);

			return R;
		}
	};
	//
	//
	//
	public        PipelineLogic        pipelineLogic;
	public        CompilationRunner    __cr;
	private       CompilerInstructions rootCI;
	private       Finally              _f     = new Finally();

	public CompilationImpl2(final @NotNull ErrSink aErrSink, final IO aIO) {
		errSink            = aErrSink;
		io                 = aIO;
		_compilationNumber = new Random().nextInt(Integer.MAX_VALUE);
		pipelines          = new Pipeline(aErrSink);
	}

	@Override public void hasInstructions(final @NotNull List<CompilerInstructions> cis) throws Exception {
		assert !cis.isEmpty();

		rootCI = cis.get(0);

		__cr.start(rootCI, cfg.do_out);
	}

	@Override public void feedCmdLine(final @NotNull List<String> args) {
		feedCmdLine(args, new DefaultCompilerController());
	}

	@Override public void feedCmdLine(final List<String> args, final CompilerController ctl) {
		if (args.isEmpty()) {
			ctl.printUsage();
			return; // ab
		}

		ctl._set(this, args);
		ctl.processOptions();
		ctl.runner();

//		getFluffy().checkFinishEventuals();
		V.exit();
	}

	@Override public String getProjectName() {
		return rootCI.getName();
	}

//	@Override public Operation<OS_Module> realParseElijjahFile(final String f, final @NotNull File file, final boolean do_out) throws Exception {
//		return _use.realParseElijjahFile(f, file, do_out).success();
//	}

	//
	//
	//

	@Override public void pushItem(final CompilerInstructions aci) {
		_cis.onNext(aci);
	}

	@Override public List<ClassStatement> findClass(final String string) {
		final List<ClassStatement> l = new ArrayList<ClassStatement>();
		for (final OS_Module module : _mod.modules) {
			if (module.hasClass(string)) {
				l.add((ClassStatement) module.findClass(string));
			}
		}
		return l;
	}

	@Override public void use(final @NotNull CompilerInstructions compilerInstructions, final boolean do_out) throws Exception {
		_use.use(compilerInstructions, do_out);    // NOTE Rust
	}

	@Override public int errorCount() {
		return errSink.errorCount();
	}

	@Override public void writeLogs(final boolean aSilent, final @NotNull List<ElLog> aLogs) {
		final Multimap<String, ElLog> logMap = ArrayListMultimap.create();
		if (true) {
			for (final ElLog deduceLog : aLogs) {
				logMap.put(deduceLog.getFileName(), deduceLog);
			}
			for (final Map.Entry<String, Collection<ElLog>> stringCollectionEntry : logMap.asMap().entrySet()) {
				final F202 f202 = new F202(getErrSink(), this);
				f202.processLogs(stringCollectionEntry.getValue());
			}
		}
	}

//	public void setIO(final IO io) {
//		this.io = io;
//	}

	@Override public ErrSink getErrSink() {
		return errSink;
	}

	@Override public IO getIO() {
		return io;
	}

	//
	// region MODULE STUFF
	//

	@Override public void addModule(final OS_Module module, final String fn) {
		_mod.addModule(module, fn);
	}

	@Override public OS_Module fileNameToModule(final String fileName) {
		if (_mod.fn2m.containsKey(fileName)) {
			return _mod.fn2m.get(fileName);
		}
		return null;
	}

	// endregion

	//
	// region CLASS AND FUNCTION CODES
	//

	@Override public boolean getSilence() {
		return cfg.silent;
	}

	@Override public Operation2<OS_Module> findPrelude(final String prelude_name) {
		return _use.findPrelude(prelude_name);
	}

	@Override public void addFunctionMapHook(final FunctionMapHook aFunctionMapHook) {
		getDeducePhase().addFunctionMapHook(aFunctionMapHook);
	}

	@Override public @NotNull DeducePhase getDeducePhase() {
		// TODO subscribeDeducePhase??
		return pipelineLogic.dp;
	}

	@Override public int nextClassCode() {
		return _repo.nextClassCode();
	}

	// endregion

	//
	// region PACKAGES
	//

	@Override public int nextFunctionCode() {
		return _repo.nextFunctionCode();
	}

	@Override public OS_Package getPackage(final Qualident pkg_name) {
		return _repo.getPackage(pkg_name.toString());
	}

	// endregion

	@Override public OS_Package makePackage(final Qualident pkg_name) {
		return _repo.makePackage(pkg_name);
	}

	@Override public int compilationNumber() {
		return _compilationNumber;
	}

	@Override public String getCompilationNumberString() {
		return String.format("%08x", _compilationNumber);
	}

	@Override@Deprecated
	public int modules_size() {
		return _mod.size();
	}

	@Override public @NotNull List<GeneratedNode> getLGC() {
		return getDeducePhase().generatedClasses.copy();
	}

	@Override public boolean isPackage(final String aPackageName) {
		return _repo.isPackage(aPackageName);
	}

	@Override public Pipeline getPipelines() {
		return pipelines;
	}

	@Override public ModuleBuilder moduleBuilder() {
		return new ModuleBuilder(this);
	}

	@Override public Finally reports() {
		return _f;
	}

	static class MOD {
		final         List<OS_Module>        modules = new ArrayList<OS_Module>();
		private final Map<String, OS_Module> fn2m    = new HashMap<String, OS_Module>();
//		private final Compilation            c;

		public MOD(final Compilation aCompilation) {
//			c = aCompilation;
		}

		public void addModule(final OS_Module module, final String fn) {
			modules.add(module);
			fn2m.put(fn, module);
		}

		public int size() {
			return modules.size();
		}

		public List<OS_Module> modules() {
			return modules;
		}
	}

	//
	//
	//
	static class CompilationConfig {
		public    boolean do_out;
		public    Stages  stage  = Stages.O; // Output
		protected boolean silent = false;
		boolean showTree = false;
	}

	static class CIS implements Observer<CompilerInstructions> {

		private final Subject<CompilerInstructions> compilerInstructionsSubject = ReplaySubject.create();
		CompilerInstructionsObserver _cio;

		@Override
		public void onSubscribe(@NonNull final Disposable d) {
			compilerInstructionsSubject.onSubscribe(d);
		}

		@Override
		public void onNext(@NonNull final CompilerInstructions aCompilerInstructions) {
			compilerInstructionsSubject.onNext(aCompilerInstructions);
		}

		@Override
		public void onError(@NonNull final Throwable e) {
			compilerInstructionsSubject.onError(e);
		}

		@Override
		public void onComplete() {
			throw new IllegalStateException();
			//compilerInstructionsSubject.onComplete();
		}

		public void almostComplete() {
			_cio.almostComplete();
		}

		public void subscribe(final Observer<CompilerInstructions> aCio) {
			compilerInstructionsSubject.subscribe(aCio);
		}
	}

	public static class CompilationAlways {
		public static boolean VOODOO = false;

		@NotNull
		public static String defaultPrelude() {
			return "c";
		}
	}

}

//
//
//

package tripleo.elijah.comp;

public interface CompilationChange {
    void apply(final Compilation c);
}

package tripleo.elijah.comp;

import tripleo.elijah.comp.specs.ElijahCache;
import tripleo.elijah.comp.specs.ElijahSpec;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.nextgen.query.Mode;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;
import tripleo.elijah_gontec.service.GonCompilationService;
import tripleo.elijah_gontec.service.GonService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class CX_ParseElijahFile {

	public static Operation<OS_Module> parseAndCache(final ElijahSpec aSpec, final ElijahCache aElijahCache, final String absolutePath, final Compilation compilation) {
		final Operation<OS_Module> calm;
		try {
			calm = parseElijahFile_(aSpec, compilation, aElijahCache, new File(absolutePath), compilation);
		} catch (final IOException aE) {
			return Operation.failure(aE);
		}

		if (calm.mode() == Mode.SUCCESS) {
			aElijahCache.put(aSpec, absolutePath, calm.success());
		}

		return calm;
	}

	private static Operation<OS_Module> parseElijahFile_(final ElijahSpec spec,
	                                                     final Compilation aCompilation,
	                                                     final ElijahCache aElijahCache,
	                                                     final File file,
	                                                     final Compilation c) throws IOException {
		final IO io = aCompilation.getIO();

		// tree add something

		final String    f      = spec.f();
		final boolean   do_out = spec.do_out();
		final OS_Module R;

		try (final InputStream s = io.readFile(file)) {
			final String absolutePath = file.getCanonicalPath();

			final Operation<OS_Module> om = parseElijahFile(f, s, do_out, c, absolutePath);
			if (om.mode() == Mode.FAILURE) {
				final Exception e = om.failure();
				assert e != null;

				SimplePrintLoggerToRemoveSoon.println_err2(("parser exception: " + e));
				e.printStackTrace(System.err);
				s.close();
				return Operation.failure(e);
			}
			R = om.success();
		}
		return Operation.success(R);
	}

	public static Operation<OS_Module> parseElijahFile(final String f, final InputStream s, final boolean do_out, final Compilation compilation, final String absolutePath) {
		GonCompilationService cs = (GonCompilationService) GonService.load("GonCompilationService");
		if (cs == null) {
			final Throwable[] eee = new Throwable[1];
			final OS_Module[] mmm = new OS_Module[1];

			GonResponseChannel rc = cs.submit(f, s);
			rc.read(OS_Module.class, (OS_Module mm) -> mmm[0] = mm);
			rc.readError(null, (Throwable t) -> {
				eee[0] = t;
			});
			rc.waitResponse();

			if (eee[0] == null) {
				final OS_Module module = mmm[0];
				module.setFileName(absolutePath);
				return Operation.success(module);
			} else {
				return Operation.failure((Exception) eee[0]);
			}
		}
		return Operation.failure(new Exception("service failed to load"));
	}

	private static Operation<OS_Module> parseElijahFile(final ElijahSpec spec, final Compilation compilation) {
		final var absolutePath = new File(spec.f()).getAbsolutePath(); // !!
		return parseElijahFile(spec.f(), spec.s(), spec.do_out(), compilation, absolutePath);
	}
}

package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.lang.AccessNotation;
import tripleo.elijah.lang.NamespaceStatement;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.gen_generic.CodeGenerator;
import tripleo.elijah.stages.gen_generic.IDependencyReferent;
import tripleo.elijah.stages.gen_generic.IGenerateResult;

import javax.management.relation.Role;

public interface IGeneratedNamespace extends DependencyTracker, GeneratedContainer, IDependencyReferent, GNCoded {
    void addAccessNotation(AccessNotation an);

    void createCtor0();

    default boolean getPragma(String auto_construct) { // TODO this should be part of Context
        return false;
    }

    String getName();

    OS_Element getElement();

    NamespaceStatement getNamespaceStatement();

    @Nullable IVarTableEntry getVariable(String aVarName);

    void generateCode(CodeGenerator aCodeGenerator, IGenerateResult aGr);

    @NotNull String identityString();

    OS_Module module();

    Role getRole();
}

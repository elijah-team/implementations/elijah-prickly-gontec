package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.comp.ErrSink;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.NormalTypeName;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.lang.TypeName;
import tripleo.elijah.lang.types.OS_FuncType;
import tripleo.elijah.lang.types.OS_UserClassType;

public interface IGenType {
    ClassInvocation genCI(TypeName aGenericTypeName,
                          DeduceTypes2 deduceTypes2,
                          ErrSink errSink,
                          DeducePhase phase);

    @Override
    boolean equals(Object aO);

    @Override
    int hashCode();

    String asString();

    void set(@NotNull OS_Type aType);

    boolean isNull();

    void copy(GenType aGenType);

    void genCIForGenType2(DeduceTypes2 aDeduceTypes2);

    void genNodeForGenType2();

    public static class SetGenCI {

        public ClassInvocation call(@NotNull final GenType genType, final TypeName aGenericTypeName, final @NotNull DeduceTypes2 deduceTypes2, final ErrSink errSink, final DeducePhase phase) {
            if (genType.nonGenericTypeName != null) {
                return nonGenericTypeName(genType, deduceTypes2, errSink, phase);
            }
            if (genType.resolved != null) {
                final OS_Type.Type type = genType.resolved.getType();
                switch (type) {
                    case USER_CLASS:
                        return ((OS_UserClassType) genType.resolved).resolvedUserClass(genType, aGenericTypeName, phase, deduceTypes2, errSink);
                    case FUNCTION:
                        return ((OS_FuncType) genType.resolved).resolvedFunction(genType, aGenericTypeName, deduceTypes2, errSink, phase);
                    case FUNC_EXPR:
                        // TODO what to do here?
                        final int y = 2;
                        break;
                }
            }
            return null;
        }

        private @NotNull ClassInvocation nonGenericTypeName(final @NotNull GenType genType, final DeduceTypes2 deduceTypes2, final ErrSink errSink, final DeducePhase phase) {
            @NotNull final NormalTypeName aTyn1 = (NormalTypeName) genType.nonGenericTypeName;
            @Nullable final String constructorName = null; // TODO this comes from nowhere

            switch (genType.resolved.getType()) {
                case OS_Type.Type.GENERIC_TYPENAME:
                    final int y = 2; // TODO seems to not be necessary
                    assert false;
                    return null;
                case OS_Type.Type.USER_CLASS:
                    final @NotNull ClassStatement best = genType.resolved.getClassOf();
                    //
                    ClassInvocation clsinv2 = DeduceTypes2.ClassInvocationMake.withGenericPart(best, constructorName, aTyn1, deduceTypes2, errSink);
                    clsinv2 = phase.registerClassInvocation(clsinv2);
                    genType.ci = clsinv2;
                    return clsinv2;
                default:
                    throw new IllegalStateException("Unexpected value: " + genType.resolved.getType());
            }
        }

    }
}

package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.BaseFunctionDef;
import tripleo.elijah.stages.gen_generic.IDependencyReferent;

public interface IGeneratedConstructor extends DependencyTracker, GeneratedNode, DeduceTypes2.ExpectationBase, IDependencyReferent {
    void setFunctionInvocation(FunctionInvocation fi);

    @Override
    String toString();

    String name();

    String identityString();

    @NotNull BaseFunctionDef getFD();

    VariableTableEntry getSelf();
}

package tripleo.elijah.stages.gen_fn;

import org.jdeferred2.DoneCallback;
import org.jdeferred2.Promise;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.*;

import java.util.Collection;

public interface IIdentTableEntry extends Constructable, TableEntryIV, DeduceTypes2.ExpectationBase {
    OS_Element getResolvedElement();

    void addPotentialType(int instructionIndex, ITypeTableEntry tte);

    @Override
    @NotNull String toString();

    boolean isResolved();

    GeneratedNode resolvedType();

    boolean hasResolvedElement();

    int getIndex();

    Context getPC();

    void onType(@NotNull DeducePhase phase, OnType callback);

    //	@SuppressFBWarnings("NP_NONNULL_RETURN_VIOLATION")
    @NotNull Collection<ITypeTableEntry> potentialTypes();

    void setConstructable(ProcTableEntry aPte);

    void resolveTypeToClass(GeneratedNode gn);

    void setGenType(GenType aGenType);

    Promise<ProcTableEntry, Void, Void> constructablePromise();

    void setGenType(GenType genType, BaseGeneratedFunction gf);

    void makeType(BaseGeneratedFunction aGeneratedFunction, ITypeTableEntry.Type aType, OS_Type aOS_Type);

    IdentExpression getIdent();

    void setDeduceTypes2(@NotNull DeduceTypes2 aDeduceTypes2, Context aContext, @NotNull BaseGeneratedFunction aGeneratedFunction);

    DeducePath buildDeducePath(BaseGeneratedFunction generatedFunction);

    void fefiDone(GenType aGenType);

    String expectationString();

    Promise<InstructionArgument, Void, Void> backlinkSet();

    void onFefiDone(DoneCallback<GenType> aCallback);

    InstructionArgument getBacklink();

    void setBacklink(InstructionArgument aBacklink);

    void makeType(BaseGeneratedFunction aGeneratedFunction, ITypeTableEntry.Type aType, IExpression aExpression);

    IDeduceElement3 getDeduceElement3(DeduceTypes2 aDeduceTypes2, BaseGeneratedFunction aGeneratedFunction);

    ITE_Zero zero();
}

package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.BaseFunctionDef;
import tripleo.elijah.stages.deduce.DeduceTypes2;
import tripleo.elijah.stages.deduce.IDeduceTypes2;
import tripleo.elijah.stages.gen_generic.IDependencyReferent;

public interface IGeneratedFunction extends DependencyTracker, GeneratedNode, IDeduceTypes2.ExpectationBase, IDependencyReferent, GNCoded {
	String name();

	@Override
	String identityString();

	@NotNull BaseFunctionDef getFD();

	IVariableTableEntry getSelf();

	@Override
	Role getRole();
}

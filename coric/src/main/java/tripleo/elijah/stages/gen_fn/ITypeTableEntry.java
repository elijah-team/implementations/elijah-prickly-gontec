package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.GenericTypeName;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.lang.TypeName;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;

public interface ITypeTableEntry {
    @Override
    @NotNull String toString();

    int getIndex();

    void resolve(GeneratedNode aResolved);

    GeneratedNode resolved();

    boolean isResolved();

    OS_Type getAttached();

    void setAttached(OS_Type aAttached);

    void setAttached(GenType aGenType);

    void addSetAttached(OnSetAttached osa);

    void genTypeCI(ClassInvocation aClsinv);

    public enum Type {
        SPECIFIED, TRANSIENT
    }

    public interface OnSetAttached {
        void onSetAttached(TypeTableEntry aTypeTableEntry);
    }
}

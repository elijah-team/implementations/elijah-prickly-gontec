package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.entrypoints.EntryPoint;
import tripleo.elijah.entrypoints.EntryPointList;
import tripleo.elijah.lang.*;
import tripleo.elijah.stages.deduce.IClassInvocation;
import tripleo.elijah.stages.deduce.FunctionInvocation;
import tripleo.elijah.stages.instructions.InstructionArgument;
import tripleo.elijah.stages.instructions.InstructionName;

import java.util.List;

public interface IGenerateFunctions {
    @Deprecated
    void generateFromEntryPoints(List<EntryPoint> entryPoints, IDeducePhase dp);

    void generateFromEntryPoints(EntryPointList epl, IDeducePhase deducePhase);

    @NotNull IGeneratedConstructor generateConstructor(ConstructorDef aConstructorDef, ClassStatement parent, // TODO Namespace constructors
                                                      FunctionInvocation aFunctionInvocation);

    @NotNull IGeneratedConstructor generateConstructor_000(ConstructorDef aConstructorDef, ClassStatement parent, // TODO Namespace constructors
                                                          FunctionInvocation aFunctionInvocation);

    @NotNull IGeneratedClass generateClass(@NotNull ClassStatement klass);

    IGeneratedClass generateClass(ClassStatement aClassStatement, IClassInvocation aClassInvocation);

    @NotNull IGeneratedFunction generateFunction(@NotNull FunctionDef fd, OS_Element parent, @NotNull FunctionInvocation aFunctionInvocation);

    void generateAllTopLevelClasses(List<IGeneratedNode> lgc);

    @NotNull IGeneratedNamespace generateNamespace(NamespaceStatement namespace1);

    void generate_item_assignment(StatementWrapper aStatementWrapper, @NotNull IExpression x, @NotNull BaseIGeneratedFunction gf, @NotNull Context cctx);

    InstructionArgument simplify_expression(@NotNull IExpression expression, @NotNull BaseIGeneratedFunction gf, Context cctx);

    int addProcTableEntry(IExpression expression, InstructionArgument expression_num, List<TypeTableEntry> args, BaseIGeneratedFunction gf);

    @NotNull List<TypeTableEntry> get_args_types(@Nullable ExpressionList args, BaseIGeneratedFunction gf, @NotNull Context aContext);

    class S1toG_GC_Processor {
        private final GenerateFunctions gf;

        public S1toG_GC_Processor(final GenerateFunctions generateFunctions) {
            this.gf = generateFunctions;
        }

        public int add_i(final IGeneratedConstructor gf2, final InstructionName e, final List<InstructionArgument> lia, final Context cctx) {
            return gf.add_i(gf2, e, lia, cctx);
        }

        public void generate_item(final OS_Element item, final IGeneratedConstructor gf2, final Context cctx) {
            gf.generate_item(item, gf2, cctx);
        }
    }
}

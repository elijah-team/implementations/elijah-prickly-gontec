package tripleo.elijah.stages.gen_fn;

import org.jdeferred2.Promise;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.Context;
import tripleo.elijah.stages.deduce.DeduceTypes2;
import tripleo.elijah.stages.deduce.IDeduceTypes2;
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3;
import tripleo.elijah.stages.deduce.post_bytecode.PostBC_Processor;
import tripleo.elijah.stages.deduce.zero.VTE_Zero;

import java.util.Collection;

public interface IVariableTableEntry extends Constructable, TableEntryIV, IDeduceTypes2.ExpectationBase {
	String getName();

	@NotNull Collection<ITypeTableEntry> potentialTypes();

	int getIndex();

	@Override
	@NotNull String toString();

	Promise<IGenType, Void, Void> typePromise();

	boolean typeDeferred_isPending();

	void addPotentialType(int instructionIndex, ITypeTableEntry tte);

	GeneratedNode resolvedType();

	@Override
	void setConstructable(IProcTableEntry aPte);

	@Override
//	public void setConstructable(final ProcTableEntry aPte) {
//		constructable_pte = aPte;
//	}

//	@Override
	void resolveTypeToClass(GeneratedNode aNode);

	void setGenType(IGenType aGenType);

	void resolveType(@NotNull IGenType aGenType);

	@Override
	Promise<IProcTableEntry, Void, Void> constructablePromise();

	@Override
	@NotNull String expectationString();

	void setDeduceTypes2(IDeduceTypes2 aDeduceTypes2, Context aContext, IBaseGeneratedFunction aGeneratedFunction);

	void resolve_var_table_entry_for_exit_function();

	boolean typeDeferred_isResolved();

	PostBC_Processor getPostBC_Processor(Context aFd_ctx, IDeduceTypes2.DeduceClient1 aDeduceClient1);

	IDeduceElement3 getDeduceElement3();

	VTE_Zero zero();

	void setLikelyType(GenType aGenType);
}

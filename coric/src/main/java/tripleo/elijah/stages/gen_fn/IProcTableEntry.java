package tripleo.elijah.stages.gen_fn;

import org.jdeferred2.DoneCallback;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.comp.ErrSink;
import tripleo.elijah.lang.Context;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.stages.deduce.*;
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3;
import tripleo.elijah.stages.deduce.zero.IPTE_Zero;
import tripleo.elijah.stages.deduce.zero.PTE_Zero;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah_prolific.deduce.DT_Element3;

import java.util.List;
import java.util.function.Consumer;

public interface IProcTableEntry extends IIdentTableEntry, TableEntryIV {
    void onSetAttached();

    void setArgType(int aIndex, OS_Type aType);

    ClassInvocation getClassInvocation();

    void setClassInvocation(ClassInvocation aClassInvocation);

    @Override
    @NotNull String toString();

    // have no idea what this is for
    void onFunctionInvocation(DoneCallback<IFunctionInvocation> callback);

    DeferredObject<GenType, Void, Void> typeDeferred();

    Promise<GenType, Void, Void> typePromise();

    FunctionInvocation getFunctionInvocation();

    // have no idea what this is for
    void setFunctionInvocation(IFunctionInvocation aFunctionInvocation);

//    DeduceProcCall deduceProcCall();

    @NotNull String getLoggingString(@Nullable IDeduceTypes2 aDeduceTypes2, ElLog LOG);

    List<ITypeTableEntry> getArgs();

    void setDeduceTypes2(IDeduceTypes2 aDeduceTypes2, Context aContext, BaseGeneratedFunction aGeneratedFunction, ErrSink aErrSink);

    IDeduceElement3 getDeduceElement3();

    IDeduceElement3 getDeduceElement3(IDeduceTypes2 aDeduceTypes2, BaseGeneratedFunction aGeneratedFunction);

    IPTE_Zero zero();

    void onResolvedElement(Consumer<DT_Element3> cb);

    void addStatusListener(tripleo.elijah.stages.deduce.ProcTableListener aProcTableListener);

    tripleo.elijah.stages.instructions.InstructionArgument expression_num();

    public static class __DT_Element3 implements DT_Element3 {
        private final OS_Element p;
        private IDeduceTypes2 deduceTypes2;
        private BaseGeneratedFunction generatedFunction;
        private ErrSink errSink;

        public __DT_Element3(final OS_Element aP) {
            p = aP;
        }

        @Override
        public OS_Element getResolvedElement() {
            return p;
        }

        @Override
        public void setErrSink(final ErrSink aErrSink) {
            errSink = aErrSink;
        }

        @Override
        public void setGeneratedFunction(final BaseGeneratedFunction aGeneratedFunction) {
            this.generatedFunction = aGeneratedFunction;
        }

        @Override
        public void setContext(final Context aContext) {

        }

        @Override
        public void setDeduceTypes2(final IDeduceTypes2 aDeduceTypes2) {
            if (deduceTypes2 == null) {
                deduceTypes2 = aDeduceTypes2;
            }
        }

        @Override
        public void op_fail(final DTEL aDTEL) {
            System.err.println("{{DTEL}} " + aDTEL.name());
        }

        @Override
        public ErrSink getErrSink() {
            return errSink;
        }

        public BaseGeneratedFunction getGeneratedFunction() {
            return generatedFunction;
        }

        public IDeduceTypes2 getDeduceTypes2() {
            return deduceTypes2;
        }
    }
}

package tripleo.elijah.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.*;
import tripleo.elijah.stages.deduce.IDeducePhase;
import tripleo.elijah.stages.gen_generic.CodeGenerator;
import tripleo.elijah.stages.gen_generic.IDependencyReferent;
import tripleo.elijah.util.Helpers0;

import javax.management.relation.Role;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface IGeneratedClass extends DependencyTracker, GeneratedContainer, IDependencyReferent, GNCoded {
    boolean isGeneric();

    void addAccessNotation(AccessNotation an);

    void createCtor0();

    default boolean getPragma(String auto_construct) { // TODO this should be part of Context
        return false;
    }

    void addConstructor(ConstructorDef aConstructorDef, @NotNull IGeneratedConstructor aGeneratedFunction);

    boolean resolve_var_table_entries(@NotNull IDeducePhase aDeducePhase);

    @Override
    OS_Element getElement();

    ClassStatement getKlass();

    void generateCode(CodeGenerator aCodeGenerator, GenerateResult aGr);

    @Override
    String identityString();

    @Override
    OS_Module module();

    @NotNull String getNumberedName();

    @Override
    Role getRole();

    void fixupUserClasses(DeduceTypes2 aDeduceTypes2, Context aContext);

    String toString();

    @NotNull String getName();

    @NotNull
    default String getNameHelper(Map<TypeName, OS_Type> aGenericPart) {
        final List<String> ls = new ArrayList<String>();
        for (final Map.Entry<TypeName, OS_Type> entry : aGenericPart.entrySet()) { // TODO Is this guaranteed to be in order?
            final OS_Type value = entry.getValue(); // This can be another ClassInvocation using GenType
            final String name = value.getClassOf().getName();
            ls.add(name); // TODO Could be nested generics
        }
        return Helpers0.String_join(", ", ls);
    }
}

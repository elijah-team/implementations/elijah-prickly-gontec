package tripleo.elijah.stages.gen_generic;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah.stages.gen_fn.*;
import tripleo.util.buffer.Buffer;

import java.util.List;

public interface IGenerateResult {
    void addConstructor(GeneratedConstructor aGeneratedConstructor, Buffer aBuffer, TY aTY, LibraryStatementPart aLsp);

    void addFunction(BaseGeneratedFunction aGeneratedFunction, Buffer aBuffer, TY aTY, @NotNull LibraryStatementPart aLsp);

    void add(Buffer b, GeneratedNode n, TY ty, LibraryStatementPart aLsp, @NotNull Dependency d);

    void addClass(TY ty, GeneratedClass aClass, Buffer aBuf, LibraryStatementPart aLsp);

    void addNamespace(TY ty, GeneratedNamespace aNamespace, Buffer aBuf, LibraryStatementPart aLsp);

    void additional(@NotNull IGenerateResult aGgr);

    List<GenerateResultItem> results();

    public enum TY {
        HEADER, IMPL, PRIVATE_HEADER
    }
}

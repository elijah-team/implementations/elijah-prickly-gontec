package tripleo.elijah.stages.deduce.fluffy.i;

import tripleo.elijah.lang.*;

public interface FluffyComp {

	FluffyModule module(OS_Module aModule);

	void find_multiple_items(OS_Module aModule);
}

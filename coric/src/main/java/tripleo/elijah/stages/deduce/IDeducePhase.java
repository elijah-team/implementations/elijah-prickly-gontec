package tripleo.elijah.stages.deduce;

import org.jdeferred2.Promise;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.comp.Compilation;
import tripleo.elijah.diagnostic.Diagnostic;
import tripleo.elijah.lang.*;
import tripleo.elijah.nextgen.ClassDefinition;
import tripleo.elijah.stages.deduce.declarations.DeferredMember;
import tripleo.elijah.stages.deduce.declarations.DeferredMemberFunction;
import tripleo.elijah.stages.deduce.post_bytecode.State;
import tripleo.elijah.stages.gen_fn.*;
import tripleo.elijah.stages.logging.ElLog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface IDeducePhase {
    void addFunction(IGeneratedFunction generatedFunction, FunctionDef fd);

    void registerFound(FoundElement foundElement);

    void onType(IIdentTableEntry entry, OnType callback);

    void registerResolvedVariable(IdentTableEntry identTableEntry, OS_Element parent, String varName);

    void onClass(ClassStatement aClassStatement, OnClass callback);

    void addFunctionMapHook(FunctionMapHook aFunctionMapHook);

    void addDeferredMember(DeferredMember aDeferredMember);

    void addLog(ElLog aLog);

    void addDeferredMember(DeferredMemberFunction aDeferredMemberFunction);

    ClassInvocation registerClassInvocation(ClassStatement aParent);

    @Nullable
    ClassInvocation registerClassInvocation(@NotNull ClassInvocation aClassInvocation);

    boolean equivalentGenericPart(@NotNull ClassInvocation first, @NotNull ClassInvocation second);

    @NotNull
    DeduceTypes2 deduceModule(@NotNull OS_Module m, ElLog.Verbosity verbosity);

    @NotNull
    DeduceTypes2 deduceModule(@NotNull OS_Module m, @NotNull Iterable<GeneratedNode> lgf, ElLog.Verbosity verbosity);

    @NotNull
    Promise<ClassDefinition, Diagnostic, Void> generateClass(GenerateFunctions gf, ClassInvocation ci);

    State register(State aState);

    Compilation _compilation();

    void deduceModule(@NotNull OS_Module m, @NotNull Iterable<GeneratedNode> lgc, boolean _unused, ElLog.Verbosity verbosity);

    void forFunction(DeduceTypes2 deduceTypes2, @NotNull IFunctionInvocation fi, @NotNull ForFunction forFunction);

    void typeDecided(@NotNull GeneratedFunction gf, GenType aType);

    void finish(@NotNull GeneratedClasses lgc22);

    NamespaceInvocation registerNamespaceInvocation(NamespaceStatement aNamespaceStatement);

    // helper function. no generics!
    @Nullable
    ClassInvocation registerClassInvocation(ClassStatement aParent, String aO);

    default void sanityChecks() {
        for (final GeneratedNode generatedNode : generatedClasses) {
            if (generatedNode instanceof final @NotNull GeneratedClass generatedClass) {
                sanityChecks(generatedClass.functionMap.values());
//				sanityChecks(generatedClass.constructors.values()); // TODO reenable
            } else if (generatedNode instanceof final @NotNull GeneratedNamespace generatedNamespace) {
                sanityChecks(generatedNamespace.functionMap.values());
//				sanityChecks(generatedNamespace.constructors.values());
            }
        }
    }

    default void sanityChecks(@NotNull Collection<GeneratedFunction> aGeneratedFunctions) {
        for (@NotNull final GeneratedFunction generatedFunction : aGeneratedFunctions) {
            for (@NotNull final IdentTableEntry identTableEntry : generatedFunction.idte_list) {
                switch (identTableEntry.getStatus()) {
                    case Status.UNKNOWN:
                        assert identTableEntry.getResolvedElement() == null;
                        LOG.err(String.format("250 UNKNOWN idte %s in %s", identTableEntry, generatedFunction));
                        break;
                    case Status.KNOWN:
                        assert identTableEntry.getResolvedElement() != null;
                        if (identTableEntry.type == null) {
                            LOG.err(String.format("258 null type in KNOWN idte %s in %s", identTableEntry, generatedFunction));
                        }
                        break;
                    case Status.UNCHECKED:
                        LOG.err(String.format("255 UNCHECKED idte %s in %s", identTableEntry, generatedFunction));
                        break;
                }
                for (@NotNull final ITypeTableEntry pot_tte : identTableEntry.potentialTypes()) {
                    if (pot_tte.getAttached() == null) {
                        LOG.err(String.format("267 null potential attached in %s in %s in %s", pot_tte, identTableEntry, generatedFunction));
                    }
                }
            }
        }
    }

    public static class ResolvedVariables {
        final IdentTableEntry identTableEntry;
        final OS_Element parent; // README tripleo.elijah.lang._CommonNC, but that's package-private
        final String varName;

        public ResolvedVariables(final IdentTableEntry aIdentTableEntry, final OS_Element aParent, final String aVarName) {
            assert aParent instanceof ClassStatement || aParent instanceof NamespaceStatement;

            identTableEntry = aIdentTableEntry;
            parent = aParent;
            varName = aVarName;
        }
    }

    public static class GeneratedClasses implements Iterable<GeneratedNode> {
        @NotNull
        public List<GeneratedNode> generatedClasses = new ArrayList<GeneratedNode>();

        public void add(final GeneratedNode aClass) {
            generatedClasses.add(aClass);
        }

        @Override
        public Iterator<GeneratedNode> iterator() {
            return generatedClasses.iterator();
        }

        public int size() {
            return generatedClasses.size();
        }

        public List<GeneratedNode> copy() {
            return new ArrayList<GeneratedNode>(generatedClasses);
        }

        public void addAll(final List<GeneratedNode> lgc) {
            // TODO is this method really needed
            generatedClasses.addAll(lgc);
        }

        public List<IGeneratedClass> filterClassesByModule(final OS_Module aModule) {
            return filterClasses(c -> c.module() == aModule);
        }

        public List<IGeneratedClass> filterClasses(final Predicate<IGeneratedClass> pgc) {
            return generatedClasses
                    .stream()
                    .filter(x -> {
                        if (x instanceof GeneratedClass) {
                            return pgc.test((IGeneratedClass) x);
                        } else {
                            return false;
                        }
                    })
                    .map(x -> (IGeneratedClass) x)
                    .collect(Collectors.toList());
        }
    }
}

package tripleo.elijah.stages.deduce;

import org.jdeferred2.DoneCallback;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.comp.ErrSink;
import tripleo.elijah.contexts.ClassContext;
import tripleo.elijah.lang.*;
import tripleo.elijah.lang.types.*;
import tripleo.elijah.lang2.BuiltInTypes;
import tripleo.elijah.lang2.ElElementVisitor;
import tripleo.elijah.stages.gen_fn.*;
import tripleo.elijah.stages.instructions.InstructionArgument;
import tripleo.elijah.stages.instructions.InstructionName;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;
import tripleo.elijah.work.WorkJob;
import tripleo.elijah.work.WorkList;
import tripleo.elijah_prolific.v.V;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface IDeduceTypes2 {
    void deduceFunctions(@NotNull Iterable<GeneratedNode> lgf);

    boolean deduceOneFunction(@NotNull IGeneratedFunction aGeneratedFunction, @NotNull IDeducePhase aDeducePhase);

    IDeducePhase _phase();

    ErrSink _errSink();

    void onFinish(Runnable r);


    @NotNull IFunctionInvocation newFunctionInvocation(BaseFunctionDef aFunctionDef,
                                                       IProcTableEntry aPte,
                                                       @NotNull IInvocation aInvocation,
                                                       @NotNull IDeducePhase aDeducePhase);





    void deduceClasses(@NotNull List<IGeneratedClass> matching_class_list);

    @Nullable IClassInvocation genCI(@NotNull IGenType IGenType, TypeName aGenericTypeName);

    @NotNull IGenType resolve_type(@Nullable OS_Type type, Context ctx) throws IResolveError;

    @NotNull IGenType resolve_type(OS_Module module, @Nullable OS_Type type, Context ctx) throws IResolveError;

    String getFileName();

    @NotNull IGenerateFunctions getGenerateFunctions(@NotNull OS_Module aModule);

    void resolve_ident_table_entry(@NotNull IIdentTableEntry ite, @NotNull IBaseGeneratedFunction generatedFunction, Context ctx);

    boolean deduceOneConstructor(@NotNull IGeneratedConstructor aGeneratedConstructor, @NotNull DeducePhase aDeducePhase);

    void deduce_generated_constructor(IGeneratedConstructor generatedFunction);

    void deduce_generated_function_base(@NotNull IBaseGeneratedFunction generatedFunction, @NotNull BaseFunctionDef fd);

    void do_assign_normal(@NotNull IBaseGeneratedFunction generatedFunction, Context aFd_ctx, @NotNull Instruction instruction, Context aContext);

    void do_assign_normal_ident_deferred(@NotNull IBaseGeneratedFunction generatedFunction,
                                         @NotNull Context aContext,
                                         @NotNull IIdentTableEntry aIdentTableEntry);

    default void do_assign_normal_ident_deferred_FALI(IBaseGeneratedFunction generatedFunction, IIdentTableEntry aIdentTableEntry, FormalArgListItem fali) {
        final IGenType IGenType = new IGenType();
        final IInvocation invocation;
        if (generatedFunction.fi.getClassInvocation() != null) {
            invocation = generatedFunction.fi.getClassInvocation();
            IGenType.resolved = ((IClassInvocation) invocation).getKlass().getOS_Type();
        } else {
            invocation = generatedFunction.fi.getNamespaceInvocation();
            IGenType.resolvedn = ((INamespaceInvocation) invocation).getNamespace();
        }
        IGenType.ci = invocation;
        final @Nullable InstructionArgument vte_ia = generatedFunction.vte_lookup(fali.name());
        assert vte_ia != null;
        ((IntegerIA) vte_ia).getEntry().typeResolvePromise().then(new DoneCallback<IGenType>() {
            @Override
            public void onDone(final IGenType result) {
                assert result.resolved != null;
                aIdentTableEntry.type.setAttached(result.resolved);
            }
        });
        generatedFunction.addDependentType(IGenType);
    }

    void do_assign_normal_ident_deferred_VariableStatement(@NotNull IBaseGeneratedFunction generatedFunction, @NotNull IIdentTableEntry aIdentTableEntry, @NotNull VariableStatement vs);

    default void implement_is_a(@NotNull IBaseGeneratedFunction gf, @NotNull Instruction instruction) {
        final IntegerIA testing_var_ = (IntegerIA) instruction.getArg(0);
        final IntegerIA testing_type_ = (IntegerIA) instruction.getArg(1);
        final Label target_label = ((LabelIA) instruction.getArg(2)).label;

        final VariableTableEntry testing_var = gf.getVarTableEntry(testing_var_.getIndex());
        final TypeTableEntry testing_type__ = gf.getTypeTableEntry(testing_type_.getIndex());

        final IGenType IGenType = testing_type__.IGenType;
        if (IGenType.resolved == null) {
            try {
                IGenType.resolved = resolve_type(IGenType.typeName, gf.getFD().getContext()).resolved;
            } catch (final IResolveError aResolveError) {
//				aResolveError.printStackTrace();
                errSink.reportDiagnostic(aResolveError);
                return;
            }
        }
        if (IGenType.ci == null) {
            IGenType.genCI(IGenType.nonGenericTypeName, this, errSink, phase);
        }
        if (IGenType.node == null) {
            if (IGenType.ci instanceof IClassInvocation) {
                final WlGenerateClass gen = new WlGenerateClass(getGenerateFunctions(module), (IClassInvocation) IGenType.ci, phase.generatedClasses, phase.codeRegistrar);
                gen.run(null);
                IGenType.node = gen.getResult();
            } else if (IGenType.ci instanceof INamespaceInvocation) {
                final WlGenerateNamespace gen = new WlGenerateNamespace(getGenerateFunctions(module), (INamespaceInvocation) IGenType.ci, phase.generatedClasses, phase.codeRegistrar);
                gen.run(null);
                IGenType.node = gen.getResult();
            }
        }
        final GeneratedNode testing_type = testing_type__.resolved();
        assert testing_type != null;
    }

    void onEnterFunction(@NotNull IBaseGeneratedFunction generatedFunction, Context aContext);

    default void resolve_cte_expression(@NotNull ConstantTableEntry cte, Context aContext) {
        final IExpression initialValue = cte.initialValue;
        switch (initialValue.getKind()) {
            case NUMERIC:
                resolve_cte_expression_builtin(cte, aContext, BuiltInTypes.SystemInteger);
                break;
            case STRING_LITERAL:
                resolve_cte_expression_builtin(cte, aContext, BuiltInTypes.String_);
                break;
            case CHAR_LITERAL:
                resolve_cte_expression_builtin(cte, aContext, BuiltInTypes.SystemCharacter);
                break;
            case IDENT: {
                final OS_Type a = cte.getTypeTableEntry().getAttached();
                if (a != null) {
                    assert a.getType() != null;
                    if (a.getType() == OS_Type.Type.BUILT_IN && a.getBType() == BuiltInTypes.Boolean) {
                        assert BuiltInTypes.isBooleanText(cte.getName());
                    } else
                        throw new NotImplementedException();
                } else {
                    assert false;
                }
                break;
            }
            default: {
                LOG.err("8192 " + initialValue.getKind());
                throw new NotImplementedException();
            }
        }
    }

    /*static*/
    @NotNull Promise<IGenType, IResolveError, Void> resolve_type_p(OS_Module module, @NotNull OS_Type type, Context ctx);

    default void resolve_cte_expression_builtin(@NotNull ConstantTableEntry cte, Context aContext, BuiltInTypes aBuiltInType) {
        final OS_Type a = cte.getTypeTableEntry().getAttached();
        if (a == null || a.getType() != OS_Type.Type.USER_CLASS) {
            try {
                cte.getTypeTableEntry().setAttached(resolve_type(new OS_BuiltinType(aBuiltInType), aContext));
            } catch (final IResolveError IResolveError) {
                SimplePrintLoggerToRemoveSoon.println2("117 Can't be here");
//				IResolveError.printStackTrace(); // TODO print diagnostic
            }
        }
    }

    void onExitFunction(@NotNull IBaseGeneratedFunction generatedFunction, Context aFd_ctx, Context aContext);

    @NotNull DeferredMemberFunction deferred_member_function(OS_Element aParent,
                                                             @Nullable IInvocation aInvocation,
                                                             BaseFunctionDef aFunctionDef,
                                                             IFunctionInvocation aFunctionInvocation);

    void resolve_function_return_type(@NotNull IBaseGeneratedFunction generatedFunction);

    /* *
     * See {@link Implement_construct#_implement_construct_typ e}
     */
    @Nullable
    default IClassInvocation genCI(@NotNull TypeTableEntry aType) {
        final IGenType IGenType = aType.IGenType;
        if (IGenType.nonGenericTypeName != null) {
            @NotNull final NormalTypeName aTyn1 = (NormalTypeName) IGenType.nonGenericTypeName;
            @Nullable final String constructorName = null; // TODO this comes from nowhere
            final ClassStatement best = IGenType.resolved.getClassOf();
            //
            @NotNull final List<TypeName> gp = best.getGenericPart();
            @Nullable IClassInvocation clsinv = new IClassInvocation(best, constructorName);
            if (gp.size() > 0) {
                final TypeNameList gp2 = aTyn1.getGenericPart();
                for (int i = 0; i < gp.size(); i++) {
                    final TypeName typeName = gp2.get(i);
                    @NotNull final IGenType genType1;
                    try {
                        genType1 = resolve_type(new OS_UserType(typeName), typeName.getContext());
                        clsinv.set(i, gp.get(i), genType1.resolved);
                    } catch (final IResolveError aResolveError) {
                        aResolveError.printStackTrace();
                        return null;
                    }
                }
            }
            clsinv = phase.registerClassInvocation(clsinv);
            IGenType.ci = clsinv;
            return clsinv;
        }
        if (IGenType.resolved != null) {
            final ClassStatement best = IGenType.resolved.getClassOf();
            @Nullable final String constructorName = null; // TODO what to do about this, nothing I guess

            @NotNull final List<TypeName> gp = best.getGenericPart();
            @Nullable IClassInvocation clsinv = new IClassInvocation(best, constructorName);
            assert best.getGenericPart().size() == 0;
/*
            if (gp.size() > 0) {
                TypeNameList gp2 = aTyn1.getGenericPart();
                for (int i = 0; i < gp.size(); i++) {
                    final TypeName typeName = gp2.get(i);
                    @NotNull OS_Type typeName2;
                    try {
                        typeName2 = resolve_type(new OS_Type(typeName), typeName.getContext());
                        clsinv.set(i, gp.get(i), typeName2);
                    } catch (IResolveError aResolveError) {
                        aResolveError.printStackTrace();
                        return null;
                    }
                }
            }
*/
            clsinv = phase.registerClassInvocation(clsinv);
            IGenType.ci = clsinv;
            return clsinv;
        }
        return null;
    }

    void resolveIdentIA2_(@NotNull Context context, @NotNull IdentIA identIA, @NotNull GeneratedFunction generatedFunction, @NotNull FoundElement foundElement);

    void resolveIdentIA2_(@NotNull Context ctx,
                          @Nullable IdentIA identIA,
                          @Nullable List<InstructionArgument> s,
                          @NotNull IBaseGeneratedFunction generatedFunction,
                          @NotNull FoundElement foundElement);

    OS_Type gt(@NotNull IGenType aType);

    @NotNull
    default ArrayList<ITypeTableEntry> getPotentialTypesVte(@NotNull GeneratedFunction generatedFunction, @NotNull InstructionArgument vte_index) {
        return getPotentialTypesVte(generatedFunction.getVarTableEntry(to_int(vte_index)));
    }

    @NotNull ArrayList<ITypeTableEntry> getPotentialTypesVte(@NotNull VariableTableEntry vte);

    void resolve_var_table_entry(@NotNull VariableTableEntry vte, IBaseGeneratedFunction generatedFunction, Context ctx);

    default void do_assign_call(@NotNull IBaseGeneratedFunction generatedFunction,
                                @NotNull Context ctx,
                                @NotNull VariableTableEntry vte,
                                @NotNull FnCallArgs fca,
                                @NotNull Instruction instruction) {
        final int instructionIndex = instruction.getIndex();
        final @NotNull ProcTableEntry pte = generatedFunction.getProcTableEntry(to_int(fca.getArg(0)));
        @NotNull final IdentIA identIA = (IdentIA) pte.expression_num;

        if (vte.getStatus() == BaseTableEntry.Status.UNCHECKED) {
            pte.typePromise().then(new DoneCallback<IGenType>() {
                @Override
                public void onDone(final IGenType result) {
                    vte.resolveType(result);
                }
            });
            if (vte.getResolvedElement() != null) {
                try {
                    final OS_Element el;
                    if (vte.getResolvedElement() instanceof IdentExpression)
                        el = IDeduceLookupUtils.lookup((IdentExpression) vte.getResolvedElement(), ctx, this);
                    else
                        el = IDeduceLookupUtils.lookup(((VariableStatement) vte.getResolvedElement()).getNameToken(), ctx, this);
                    vte.setStatus(BaseTableEntry.Status.KNOWN, new GenericElementHolder(el));
                } catch (final IResolveError aResolveError) {
                    errSink.reportDiagnostic(aResolveError);
                    return;
                }
            }
        }

        if (identIA != null) {
//			LOG.info("594 "+identIA.getEntry().getStatus());

            resolveIdentIA_(ctx, identIA, generatedFunction, new FoundElement(phase) {

                final String xx = generatedFunction.getIdentIAPathNormal(identIA);

                @Override
                public void foundElement(final OS_Element e) {
//					LOG.info(String.format("600 %s %s", xx ,e));
//					LOG.info("601 "+identIA.getEntry().getStatus());
                    final OS_Element resolved_element = identIA.getEntry().getResolvedElement();
                    assert e == resolved_element;
//					set_resolved_element_pte(identIA, e, pte);
                    pte.setStatus(BaseTableEntry.Status.KNOWN, new ConstructableElementHolder(e, identIA));
                    pte.onFunctionInvocation(new DoneCallback<IFunctionInvocation>() {
                        @Override
                        public void onDone(@NotNull final FunctionInvocation result) {
                            result.generateDeferred().done(new DoneCallback<IBaseGeneratedFunction>() {
                                @Override
                                public void onDone(@NotNull final IBaseGeneratedFunction bgf) {
                                    @NotNull final PromiseExpectation<IGenType> pe = promiseExpectation(bgf, "Function Result type");
                                    bgf.onType(new DoneCallback<IGenType>() {
                                        @Override
                                        public void onDone(@NotNull final IGenType result) {
                                            pe.satisfy(result);
                                            @NotNull final TypeTableEntry tte = generatedFunction.newTypeTableEntry(ITypeTableEntry.Type.TRANSIENT, result.resolved); // TODO there has to be a better way
                                            tte.IGenType.copy(result);
                                            vte.addPotentialType(instructionIndex, tte);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }

                @Override
                public void noFoundElement() {
                    // TODO create Diagnostic and quit
                    LOG.info("1005 Can't find element for " + xx);
                }
            });
        }
        final List<ITypeTableEntry> args = pte.getArgs();
        for (int i = 0; i < args.size(); i++) {
            final TypeTableEntry tte = args.get(i); // TODO this looks wrong
//			LOG.info("770 "+tte);
            final IExpression e = tte.expression;
            if (e == null) continue;
            switch (e.getKind()) {
                case NUMERIC:
                    tte.setAttached(new OS_BuiltinType(BuiltInTypes.SystemInteger));
                    //vte.type = tte;
                    break;
                case CHAR_LITERAL:
                    tte.setAttached(new OS_BuiltinType(BuiltInTypes.SystemCharacter));
                    break;
                case IDENT:
                    do_assign_call_args_ident(generatedFunction, ctx, vte, instructionIndex, pte, i, tte, (IdentExpression) e);
                    break;
                case PROCEDURE_CALL: {
                    final @NotNull ProcedureCallExpression pce = (ProcedureCallExpression) e;
                    try {
                        final LookupResultList lrl = IDeduceLookupUtils.lookupExpression(pce.getLeft(), ctx, this);
                        @Nullable OS_Element best = lrl.chooseBest(null);
                        if (best != null) {
                            while (best instanceof AliasStatement) {
                                best = IDeduceLookupUtils._resolveAlias2((AliasStatement) best, this);
                            }
                            if (best instanceof FunctionDef) {
                                final OS_Element parent = best.getParent();
                                @Nullable final IInvocation invocation;
                                if (parent instanceof NamespaceStatement) {
                                    invocation = phase.registerNamespaceInvocation((NamespaceStatement) parent);
                                } else if (parent instanceof ClassStatement) {
                                    @NotNull final IClassInvocation ci = new IClassInvocation((ClassStatement) parent, null);
                                    invocation = phase.registerClassInvocation(ci);
                                } else
                                    throw new NotImplementedException(); // TODO implement me

                                forFunction(newFunctionInvocation((FunctionDef) best, pte, invocation, phase), new ForFunction() {
                                    @Override
                                    public void typeDecided(@NotNull final IGenType aType) {
                                        tte.setAttached(gt(aType)); // TODO stop setting attached!
                                        tte.IGenType.copy(aType);
//										vte.addPotentialType(instructionIndex, tte);
                                    }
                                });
//								tte.setAttached(new OS_FuncType((FunctionDef) best));

                            } else {
                                final int y = 2;
                                throw new NotImplementedException();
                            }
                        } else {
                            final int y = 2;
                            throw new NotImplementedException();
                        }
                    } catch (final IResolveError aResolveError) {
//					aResolveError.printStackTrace();
//					final int y = 2;
//					throw new NotImplementedException();
                        V.asv(V.e.DT2_1785, "" + aResolveError);
                    }
                }
                break;
                case DOT_EXP: {
                    final @NotNull DotExpression de = (DotExpression) e;
                    try {
                        final LookupResultList lrl = IDeduceLookupUtils.lookupExpression(de.getLeft(), ctx, this);
                        @Nullable OS_Element best = lrl.chooseBest(null);
                        if (best != null) {
                            while (best instanceof AliasStatement) {
                                best = IDeduceLookupUtils._resolveAlias2((AliasStatement) best, this);
                            }
                            if (best instanceof FunctionDef) {
                                tte.setAttached(new OS_FuncType((FunctionDef) best));
                                //vte.addPotentialType(instructionIndex, tte);
                            } else if (best instanceof ClassStatement) {
                                tte.setAttached(new OS_UserClassType((ClassStatement) best));
                            } else if (best instanceof final @NotNull VariableStatement vs) {
                                @Nullable final InstructionArgument vte_ia = generatedFunction.vte_lookup(vs.getName());
                                final ITypeTableEntry tte1 = ((IntegerIA) vte_ia).getEntry().type;
                                tte.setAttached(tte1.getAttached());
                            } else {
                                final int y = 2;
                                LOG.err(best.getClass().getName());
                                throw new NotImplementedException();
                            }
                        } else {
                            final int y = 2;
                            throw new NotImplementedException();
                        }
                    } catch (final IResolveError aResolveError) {
                        aResolveError.printStackTrace();
                        final int y = 2;
                        throw new NotImplementedException();
                    }
                }
                break;

                case GET_ITEM: {
                    final @NotNull GetItemExpression gie = (GetItemExpression) e;
                    do_assign_call_GET_ITEM(gie, tte, generatedFunction, ctx);
                    continue;
                }
//				break;
                default:
                    throw new IllegalStateException("Unexpected value: " + e.getKind());
            }
        }
        {
            if (pte.expression_num == null) {
                if (fca.expression_to_call.getName() != InstructionName.CALLS) {
                    final String text = ((IdentExpression) pte.expression).getText();
                    final LookupResultList lrl = ctx.lookup(text);

                    final @Nullable OS_Element best = lrl.chooseBest(null);
                    if (best != null)
                        pte.setResolvedElement(best); // TODO do we need to add a dependency for class?
                    else {
                        errSink.reportError("Cant resolve " + text);
                    }
                } else {
                    implement_calls(generatedFunction, ctx.getParent(), instruction.getArg(1), pte, instructionIndex);
                }
            } else {
                final int y = 2;
                resolveIdentIA_(ctx, identIA, generatedFunction, new FoundElement(phase) {

                    final String x = generatedFunction.getIdentIAPathNormal(identIA);

                    @Override
                    public void foundElement(final OS_Element el) {
                        if (pte.getResolvedElement() == null)
                            pte.setResolvedElement(el);
                        if (el instanceof @NotNull final FunctionDef fd) {
                            final @Nullable IInvocation invocation;
                            if (fd.getParent() == generatedFunction.getFD().getParent()) {
                                invocation = getInvocation((GeneratedFunction) generatedFunction);
                            } else {
                                if (fd.getParent() instanceof NamespaceStatement) {
                                    final INamespaceInvocation ni = phase.registerNamespaceInvocation((NamespaceStatement) fd.getParent());
                                    invocation = ni;
                                } else if (fd.getParent() instanceof final @NotNull ClassStatement classStatement) {
                                    @Nullable IClassInvocation ci = new IClassInvocation(classStatement, null);
                                    final @NotNull List<TypeName> genericPart = classStatement.getGenericPart();
                                    if (genericPart.size() > 0) {
                                        // TODO handle generic parameters somehow (getInvocationFromBacklink?)

                                    }
                                    ci = phase.registerClassInvocation(ci);
                                    invocation = ci;
                                } else
                                    throw new NotImplementedException();
                            }
                            forFunction(newFunctionInvocation(fd, pte, invocation, phase), new ForFunction() {
                                @Override
                                public void typeDecided(@NotNull final IGenType aType) {
                                    if (!vte.typeDeferred_isPending()) {
                                        if (vte.resolvedType() == null) {
                                            final @Nullable IClassInvocation ci = genCI(aType, null);
                                            vte.type.genTypeCI(ci);
                                            ci.resolvePromise().then(new DoneCallback<IGeneratedClass>() {
                                                @Override
                                                public void onDone(final GeneratedClass result) {
                                                    vte.resolveTypeToClass(result);
                                                }
                                            });
                                        }
                                        LOG.err("2041 type already found " + vte);
                                        return; // type already found
                                    }
                                    // I'm not sure if below is ever called
                                    @NotNull final TypeTableEntry tte = generatedFunction.newTypeTableEntry(ITypeTableEntry.Type.TRANSIENT, gt(aType), pte.expression, pte);
                                    vte.addPotentialType(instructionIndex, tte);
                                }
                            });
                        } else if (el instanceof @NotNull final ClassStatement kl) {
                            @NotNull final OS_Type type = new OS_UserClassType(kl);
                            @NotNull final TypeTableEntry tte = generatedFunction.newTypeTableEntry(ITypeTableEntry.Type.TRANSIENT, type, pte.expression, pte);
                            vte.addPotentialType(instructionIndex, tte);
                            vte.setConstructable(pte);

                            register_and_resolve(vte, kl);
                        } else {
                            LOG.err("7890 " + el.getClass().getName());
                        }
                    }

                    @Override
                    public void noFoundElement() {
                        LOG.err("IdentIA path cannot be resolved " + x);
                    }
                });
            }
        }
    }

    default void do_assign_constant(@NotNull IBaseGeneratedFunction generatedFunction, @NotNull Instruction instruction, @NotNull VariableTableEntry vte, @NotNull ConstTableIA i2) {
        if (vte.type.getAttached() != null) {
            // TODO check types
        }
        final @NotNull ConstantTableEntry cte = generatedFunction.getConstTableEntry(i2.getIndex());
        if (cte.type.getAttached() == null) {
            LOG.info("Null type in CTE " + cte);
        }
//		vte.type = cte.type;
        vte.addPotentialType(instruction.getIndex(), cte.type);
    }

    default void __do_assign_call_GET_ITEM__VariableStatement(@NotNull IBaseGeneratedFunction generatedFunction,
                                                              @NotNull Context ctx,
                                                              @NotNull VariableStatement vs) throws NotImplementedException {
        final String s = vs.getName();
        @Nullable final InstructionArgument vte_ia = generatedFunction.vte_lookup(s);
        if (vte_ia != null) {
            @NotNull final VariableTableEntry vte1 = generatedFunction.getVarTableEntry(to_int(vte_ia));
            throw new NotImplementedException();
        } else {
            final IIdentTableEntry idte = generatedFunction.getIdentTableEntryFor(vs.getNameToken());
            assert idte != null;
            if (idte.type == null) return;

            @Nullable OS_Type ty = idte.type.getAttached();
            idte.onType(phase, new OnType() {
                @Override
                public void typeDeduced(final @NotNull OS_Type ty) {
                    __do_assign_call_GET_ITEM__VariableStatement__idte__typeDeduced(ty, ctx, generatedFunction, idte);
                }

                @Override
                public void noTypeFound() {
                    throw new NotImplementedException();
                }
            });
            if (ty == null) {
                @NotNull final TypeTableEntry tte3 = generatedFunction.newTypeTableEntry(
                        ITypeTableEntry.Type.SPECIFIED, new OS_UserType(vs.typeName()), vs.getNameToken());
                idte.type = tte3;
                ty = idte.type.getAttached();
            }
        }

        //				tte.attached = new OS_FuncType((FunctionDef) best); // TODO: what is this??
        //vte.addPotentialType(instructionIndex, tte);
    }

    IInvocation getInvocation(@NotNull GeneratedFunction generatedFunction);

    void do_assign_call_args_ident(@NotNull IBaseGeneratedFunction generatedFunction,
                                   @NotNull Context ctx,
                                   @NotNull VariableTableEntry vte,
                                   int aInstructionIndex,
                                   @NotNull IProcTableEntry aPte,
                                   int aI,
                                   @NotNull TypeTableEntry aTte,
                                   @NotNull IdentExpression aExpression);

    void implement_construct(IBaseGeneratedFunction generatedFunction, Instruction instruction);

    @NotNull Implement_construct newImplement_construct(IBaseGeneratedFunction generatedFunction, Instruction instruction);

    void do_assign_call_GET_ITEM(@NotNull GetItemExpression gie, ITypeTableEntry tte, @NotNull IBaseGeneratedFunction generatedFunction, Context ctx);

    default void do_assign_call(@NotNull IBaseGeneratedFunction generatedFunction,
                                @NotNull Context ctx,
                                @NotNull IIdentTableEntry idte,
                                @NotNull FnCallArgs fca,
                                int instructionIndex) {
        final @NotNull ProcTableEntry pte = generatedFunction.getProcTableEntry(to_int(fca.getArg(0)));
        for (final @NotNull TypeTableEntry tte : pte.getArgs()) {
            LOG.info("771 " + tte);
            final IExpression e = tte.expression;
            if (e == null) continue;
            switch (e.getKind()) {
                case NUMERIC: {
                    tte.setAttached(new OS_BuiltinType(BuiltInTypes.SystemInteger));
                    idte.type = tte; // TODO why not addPotentialType ? see below for example
                }
                break;
                case IDENT: {
                    final @Nullable InstructionArgument vte_ia = generatedFunction.vte_lookup(((IdentExpression) e).getText());
                    final @NotNull List<ITypeTableEntry> ll = getPotentialTypesVte((GeneratedFunction) generatedFunction, vte_ia);
                    if (ll.size() == 1) {
                        tte.setAttached(ll.get(0).getAttached());
                        idte.addPotentialType(instructionIndex, ll.get(0));
                    } else
                        throw new NotImplementedException();
                }
                break;
                default: {
                    throw new NotImplementedException();
                }
            }
        }
        {
            final String s = ((IdentExpression) pte.expression).getText();
            final LookupResultList lrl = ctx.lookup(s);
            final @Nullable OS_Element best = lrl.chooseBest(null);
            if (best != null) {
                pte.setResolvedElement(best); // TODO do we need to add a dependency for class?
            } else {
                throw new NotImplementedException();
            }
        }
    }

    default void __do_assign_call_GET_ITEM__FALI(IBaseGeneratedFunction generatedFunction,
                                                 Context ctx,
                                                 FormalArgListItem fali) {
        final String s = fali.name();
        @Nullable final InstructionArgument vte_ia = generatedFunction.vte_lookup(s);
        if (vte_ia == null) {
            return;
        }


        @NotNull final VariableTableEntry vte2 = generatedFunction.getVarTableEntry(to_int(vte_ia));

        vte2.typePromise().done(vte2_gt -> {
            //assert false; // TODO this code is never reached
            final @Nullable OS_Type ty2 = vte2_gt.typeName;
            assert ty2 != null;
            @NotNull IGenType rtype = null;
            try {
                rtype = resolve_type(ty2, ctx);
            } catch (final IResolveError IResolveError) {
                //IResolveError.printStackTrace();
                errSink.reportError("Cant resolve " + ty2); // TODO print better diagnostic
                return;
            }
            if (rtype.resolved != null && rtype.resolved.getType() == OS_Type.Type.USER_CLASS) {
                final LookupResultList lrl2 = rtype.resolved.getClassOf().getContext().lookup("__getitem__");
                @Nullable final OS_Element best2 = lrl2.chooseBest(null);
                if (best2 != null) {
                    if (best2 instanceof @Nullable final FunctionDef fd) {
                        @Nullable final ProcTableEntry pte = null;
                        final IInvocation invocation = getInvocation((GeneratedFunction) generatedFunction);
                        forFunction(newFunctionInvocation(fd, pte, invocation, phase), new ForFunction() {
                            @Override
                            public void typeDecided(final @NotNull IGenType aType) {
                                assert fd == generatedFunction.getFD();
                                //
                                @NotNull final TypeTableEntry tte1 = generatedFunction.newTypeTableEntry(ITypeTableEntry.Type.TRANSIENT, gt(aType), vte2); // TODO expression?
                                vte2.type = tte1;
                            }
                        });
                    } else {
                        throw new NotImplementedException();
                    }
                } else {
                    throw new NotImplementedException();
                }
            }
        });

/*
            if (ty2 == null) {
                @NotNull TypeTableEntry tte3 = generatedFunction.newTypeTableEntry(
                        TypeTableEntry.Type.SPECIFIED, new OS_Type(fali.typeName()), fali.getNameToken());
                vte2.type = tte3;
//						ty2 = vte2.type.attached; // TODO this is final, but why assign anyway?
            }
*/
    }

    default void __do_assign_call_GET_ITEM__VariableStatement__idte__typeDeduced(@NotNull OS_Type ty, Context ctx, IBaseGeneratedFunction generatedFunction, IIdentTableEntry idte) {
        assert ty != null;
        @NotNull IGenType rtype = null;
        try {
            rtype = resolve_type(ty, ctx);
        } catch (final IResolveError IResolveError) {
            //								IResolveError.printStackTrace();
            errSink.reportError("Cant resolve " + ty); // TODO print better diagnostic
            return;
        }
        if (rtype.resolved != null && rtype.resolved.getType() == OS_Type.Type.USER_CLASS) {
            final LookupResultList lrl2 = rtype.resolved.getClassOf().getContext().lookup("__getitem__");
            @Nullable final OS_Element best2 = lrl2.chooseBest(null);
            if (best2 != null) {
                if (best2 instanceof @NotNull final FunctionDef fd) {
                    @Nullable final ProcTableEntry pte = null;
                    final IInvocation invocation = getInvocation((GeneratedFunction) generatedFunction);
                    forFunction(newFunctionInvocation(fd, pte, invocation, phase), new ForFunction() {
                        @Override
                        public void typeDecided(final @NotNull IGenType aType) {
                            assert fd == generatedFunction.getFD();
                            //
                            if (idte.type == null) {
                                @NotNull final TypeTableEntry tte1 = generatedFunction.newTypeTableEntry(ITypeTableEntry.Type.TRANSIENT, gt(aType), idte); // TODO expression?
                                idte.type = tte1;
                            } else
                                idte.type.setAttached(gt(aType));
                        }
                    });
                } else {
                    throw new NotImplementedException();
                }
            } else {
                throw new NotImplementedException();
            }
        }
    }

    default void do_assign_constant(@NotNull IBaseGeneratedFunction generatedFunction, @NotNull Instruction instruction, @NotNull IIdentTableEntry idte, @NotNull ConstTableIA i2) {
        if (idte.type != null && idte.type.getAttached() != null) {
            // TODO check types
        }
        final @NotNull ConstantTableEntry cte = generatedFunction.getConstTableEntry(i2.getIndex());
        if (cte.type.getAttached() == null) {
            LOG.err("*** ERROR: Null type in CTE " + cte);
        }
        // idte.type may be null, but we still addPotentialType here
        idte.addPotentialType(instruction.getIndex(), cte.type);
    }

    void found_element_for_ite(IBaseGeneratedFunction generatedFunction, @NotNull IIdentTableEntry ite, @Nullable OS_Element y, Context ctx);

    @Nullable
    default IInvocation getInvocationFromBacklink(@Nullable InstructionArgument aBacklink) {
        if (aBacklink == null) return null;
        // TODO implement me
        return null;
    }

    @NotNull
    default DeferredMember deferred_member(OS_Element aParent, IInvocation aInvocation, VariableStatement aVariableStatement, @NotNull IIdentTableEntry ite) {
        @NotNull final DeferredMember dm = deferred_member(aParent, aInvocation, aVariableStatement);
        dm.externalRef().then(new DoneCallback<GeneratedNode>() {
            @Override
            public void onDone(final GeneratedNode result) {
                ite.externalRef = result;
            }
        });
        return dm;
    }

    @Nullable
    default DeferredMember deferred_member(OS_Element aParent, @Nullable IInvocation aInvocation, VariableStatement aVariableStatement) {
        if (aInvocation == null) {
            if (aParent instanceof NamespaceStatement)
                aInvocation = phase.registerNamespaceInvocation((NamespaceStatement) aParent);
        }
        @Nullable final DeferredMember dm = new DeferredMember(aParent, aInvocation, aVariableStatement);
        phase.addDeferredMember(dm);
        return dm;
    }

    @NotNull ElLog _LOG();

    void implement_calls(@NotNull IBaseGeneratedFunction gf, @NotNull Context context, InstructionArgument i2, @NotNull ProcTableEntry fn1, int pc);

    default void implement_calls_(@NotNull IBaseGeneratedFunction gf,
                                  @NotNull Context context,
                                  InstructionArgument i2,
                                  @NotNull ProcTableEntry pte,
                                  int pc) {
        final Implement_Calls_ ic = new Implement_Calls_(this, gf, context, i2, pte, pc);
        ic.action();
    }

    boolean lookup_name_calls(@NotNull Context ctx, @NotNull String pn, @NotNull ProcTableEntry pte);

    <B> @NotNull PromiseExpectation<B> promiseExpectation(ExpectationBase base, String desc);

    void resolveIdentIA_(@NotNull Context context, @NotNull IdentIA identIA, IBaseGeneratedFunction generatedFunction, @NotNull FoundElement foundElement);

    void register_and_resolve(@NotNull VariableTableEntry aVte, @NotNull ClassStatement aKlass);

    void forFunction(@NotNull IFunctionInvocation gf, @NotNull ForFunction forFunction);

    Zero_FuncExprType getZero(OS_FuncExprType aFuncExprType);

    public interface IElementProcessor {
        void elementIsNull();

        void hasElement(OS_Element el);
    }

    public interface IVariableConnector {
        void connect(VariableTableEntry aVte, String aName);
    }

    public interface ExpectationBase {
        String expectationString();
    }

    public static class Deduce_Type 

    public static class ProcessElement {
        static void processElement(final OS_Element el, final IElementProcessor ep) {
            if (el == null)
                ep.elementIsNull();
            else
                ep.hasElement(el);
        }
    }

    public static class CtorConnector implements IVariableConnector {
        private final IGeneratedConstructor IGeneratedConstructor;

        public CtorConnector(final IGeneratedConstructor aGeneratedConstructor) {
            IGeneratedConstructor = aGeneratedConstructor;
        }

        @Override
        public void connect(final IVariableTableEntry aVte, final String aName) {
            final List<GeneratedContainer.VarTableEntry> vt = ((IGeneratedClass) IGeneratedConstructor.getGenClass()).varTable;
            for (final GeneratedContainer.VarTableEntry gc_vte : vt) {
                if (gc_vte.nameToken.getText().equals(aName)) {
                    gc_vte.connect(aVte, IGeneratedConstructor);
                    break;
                }
            }
        }
    }

    public static class NullConnector implements IVariableConnector {
        @Override
        public void connect(final IVariableTableEntry aVte, final String aName) {
        }
    }

    public static class DeduceClient1 {
        private final IDeduceTypes2 dt2;

        @Contract(pure = true)
        public DeduceClient1(final IDeduceTypes2 aDeduceTypes2) {
            dt2 = aDeduceTypes2;
        }

        public @Nullable OS_Element _resolveAlias(@NotNull final AliasStatement aAliasStatement) {
        return IDeduceLookupUtils._resolveAlias(aAliasStatement, dt2);
        }

        public void found_element_for_ite(final IBaseGeneratedFunction aGeneratedFunction, @NotNull final IIdentTableEntry aIte, final OS_Element aX, final Context aCtx) {
            dt2.found_element_for_ite(aGeneratedFunction, aIte, aX, aCtx);
        }

        public @NotNull IGenType resolve_type(@NotNull final OS_Type aType, final Context aCtx) throws IResolveError {
            return dt2.resolve_type(aType, aCtx);
        }

        public @Nullable IInvocation getInvocationFromBacklink(final InstructionArgument aInstructionArgument) {
            return dt2.getInvocationFromBacklink(aInstructionArgument);
        }

        public @NotNull DeferredMember deferred_member(final OS_Element aParent, final IInvocation aInvocation, final VariableStatement aVariableStatement, @NotNull final IIdentTableEntry aIdentTableEntry) {
            return dt2.deferred_member(aParent, aInvocation, aVariableStatement, aIdentTableEntry);
        }

        public void genCI(final IGenType aResult, final TypeName aNonGenericTypeName) {
            dt2.genCI(aResult, aNonGenericTypeName);
        }

        public @Nullable IClassInvocation registerClassInvocation(final ClassStatement aClassStatement, final String aS) {
            return dt2.phase.registerClassInvocation(aClassStatement, aS);
        }

        public void genCIForGenType2(final IGenType aGenType) {
            aGenType.genCIForGenType2(dt2);
        }

        public void LOG_err(final String string) {
            dt2.LOG.err(string);
        }

        public @NotNull ArrayList<ITypeTableEntry> getPotentialTypesVte(final VariableTableEntry aVte) {
            return dt2.getPotentialTypesVte(aVte);
        }
    }

    public static class DeduceClient2 {
        private final IDeduceTypes2 IDeduceTypes2;

        public DeduceClient2(final IDeduceTypes2 IDeduceTypes2) {
            this.IDeduceTypes2 = IDeduceTypes2;
        }

        public @Nullable IClassInvocation registerClassInvocation(@NotNull final IClassInvocation ci) {
            return IDeduceTypes2.phase.registerClassInvocation(ci);
        }

        public @NotNull FunctionInvocation newFunctionInvocation(final BaseFunctionDef constructorDef, final IProcTableEntry pte, final @Nullable IClassInvocation ci) {
            return IDeduceTypes2.newFunctionInvocation(constructorDef, pte, ci, IDeduceTypes2.phase);
        }

        public INamespaceInvocation registerNamespaceInvocation(final NamespaceStatement namespaceStatement) {
            return IDeduceTypes2.phase.registerNamespaceInvocation(namespaceStatement);
        }

        public @NotNull IClassInvocation genCI(@NotNull final IGenType IGenType, final TypeName typeName) {
            return IGenType.genCI(typeName, IDeduceTypes2, IDeduceTypes2.errSink, IDeduceTypes2.phase);
        }

        public @NotNull ElLog getLOG() {
            return IDeduceTypes2.LOG;
        }
    }

    public static class GenericPart {
        private final ClassStatement classStatement;
        private final TypeName genericTypeName;

        @Contract(pure = true)
        public GenericPart(final ClassStatement aClassStatement, final TypeName aGenericTypeName) {
            classStatement = aClassStatement;
            genericTypeName = aGenericTypeName;
        }

        @Contract(pure = true)
        public boolean hasGenericPart() {
            return classStatement.getGenericPart().size() > 0;
        }

        @Contract(pure = true)
        public TypeNameList getGenericPartFromTypeName() {
            final NormalTypeName ntn = getGenericTypeName();
            return ntn.getGenericPart();
        }

        @Contract(pure = true)
        private NormalTypeName getGenericTypeName() {
            assert genericTypeName != null;
            assert genericTypeName instanceof NormalTypeName;

            return (NormalTypeName) genericTypeName;
        }
    }

    public static class ClassInvocationMake {
        public static IClassInvocation withGenericPart(final ClassStatement best,
                                                       final String constructorName,
                                                       final NormalTypeName aTyn1,
                                                       final IDeduceTypes2 dt2,
                                                       final ErrSink aErrSink) {
            @NotNull final GenericPart genericPart = new GenericPart(best, aTyn1);

            @Nullable final IClassInvocation clsinv = new IClassInvocation(best, constructorName);

            if (genericPart.hasGenericPart()) {
                final @NotNull List<TypeName> gp = best.getGenericPart();
                final @NotNull TypeNameList gp2 = genericPart.getGenericPartFromTypeName();

                for (int i = 0; i < gp.size(); i++) {
                    final TypeName typeName = gp2.get(i);
                    @NotNull final IGenType typeName2;
                    try {
                        typeName2 = dt2.resolve_type(new OS_UserType(typeName), typeName.getContext());
                        // TODO transition to IGenType
                        clsinv.set(i, gp.get(i), typeName2.resolved);
                    } catch (final IResolveError aResolveError) {
//						aResolveError.printStackTrace();
                        aErrSink.reportDiagnostic(aResolveError);
                    }
                }
            }
            return clsinv;
        }
    }

    public static class DeduceClient3 {
        private final IDeduceTypes2 IDeduceTypes2;

        public DeduceClient3(final IDeduceTypes2 aDeduceTypes2) {
            IDeduceTypes2 = aDeduceTypes2;
        }

        public ElLog getLOG() {
            return IDeduceTypes2.LOG;
        }

        public LookupResultList lookupExpression(final IExpression aExp, final Context aContext) throws IResolveError {
            return IDeduceLookupUtils.lookupExpression(aExp, aContext, IDeduceTypes2);
        }

        public IGenerateFunctions getGenerateFunctions(final OS_Module aModule) {
            return IDeduceTypes2.getGenerateFunctions(aModule);
        }

        public void resolveIdentIA2_(final Context aEctx,
                                     final IdentIA aIdentIA,
                                     final @Nullable List<InstructionArgument> aInstructionArgumentList,
                                     final IBaseGeneratedFunction aGeneratedFunction,
                                     final FoundElement aFoundElement) {
            IDeduceTypes2.resolveIdentIA2_(aEctx, aIdentIA, aInstructionArgumentList, aGeneratedFunction, aFoundElement);
        }

        public List<ITypeTableEntry> getPotentialTypesVte(final VariableTableEntry aVte) {
            return IDeduceTypes2.getPotentialTypesVte(aVte);
        }

        public IInvocation getInvocation(final GeneratedFunction aGeneratedFunction) {
            return IDeduceTypes2.getInvocation(aGeneratedFunction);
        }

        public IGenType resolve_type(final OS_Type aType, final Context aContext) throws IResolveError {
            return IDeduceTypes2.resolve_type(aType, aContext);
        }

        public DeducePhase getPhase() {
            return IDeduceTypes2.phase;
        }

        public void addJobs(final WorkJob j) {
            final @NotNull WorkList wl = new WorkList();
            wl.addJob(j);
            IDeduceTypes2.wm.addJobs(wl);
        }

        public IElementHolder newGenericElementHolderWithType(final OS_Element aElement, final TypeName aTypeName) {
            final OS_Type typeName;
            if (aTypeName.isNull())
                typeName = null;
            else
                typeName = new OS_UserType(aTypeName);
            return new GenericElementHolderWithType(aElement, typeName, IDeduceTypes2);
        }

        public void found_element_for_ite(final IBaseGeneratedFunction generatedFunction,
                                          final @NotNull IIdentTableEntry ite,
                                          final @Nullable OS_Element y,
                                          final Context ctx) {
            IDeduceTypes2.found_element_for_ite(generatedFunction, ite, y, ctx);
        }

        public void genCIForGenType2(final @NotNull IGenType IGenType) {
            IGenType.genCIForGenType2(IDeduceTypes2);
        }

        public @NotNull FunctionInvocation newFunctionInvocation(final BaseFunctionDef aFunctionDef, final ProcTableEntry aPte, final @NotNull IInvocation aInvocation) {
            return IDeduceTypes2.newFunctionInvocation(aFunctionDef, aPte, aInvocation, IDeduceTypes2.phase);
        }

        public OS_Element resolveAlias(final AliasStatement aAliasStatement) {
            try {
                final OS_Element el = IDeduceLookupUtils._resolveAlias2(aAliasStatement, IDeduceTypes2);
                return el;
            } catch (final IResolveError aE) {
                return null;
//				throw new RuntimeException(aE);
            }
        }

        public IDeduceTypes2 _dt2() {
            return IDeduceTypes2;
        }
    }

    public static class OS_SpecialVariable implements OS_Element {
        private final VariableTableEntry variableTableEntry;
        private final VariableTableType type;
        private final IBaseGeneratedFunction generatedFunction;
        public DeduceLocalVariable.MemberInvocation memberInvocation;

        public OS_SpecialVariable(final VariableTableEntry aVariableTableEntry, final VariableTableType aType, final IBaseGeneratedFunction aGeneratedFunction) {
            variableTableEntry = aVariableTableEntry;
            type = aType;
            generatedFunction = aGeneratedFunction;
        }

        @Override
        public void visitGen(final ElElementVisitor visit) {
            throw new IllegalArgumentException("not implemented");
        }

        @Override
        public Context getContext() {
            return generatedFunction.getFD().getContext();
        }

        @Override
        public OS_Element getParent() {
            return generatedFunction.getFD();
        }

        @Nullable
        public IInvocation getInvocation(final IDeduceTypes2 aDeduceTypes2) {
            final @Nullable IInvocation aInvocation;
            final OS_SpecialVariable specialVariable = this;
            assert specialVariable.type == VariableTableType.SELF;
            // first parent is always a function
            switch (DecideElObjectType.getElObjectType(specialVariable.getParent().getParent())) {
                case CLASS:
                    final ClassStatement classStatement = (ClassStatement) specialVariable.getParent().getParent();
                    aInvocation = aDeduceTypes2.phase.registerClassInvocation(classStatement, null); // TODO generics
//				ClassInvocationMake.withGenericPart(classStatement, null, null, this);
                    break;
                case NAMESPACE:
                    throw new NotImplementedException(); // README ha! implemented in
                default:
                    throw new IllegalArgumentException("Illegal object type for parent");
            }
            return aInvocation;
        }
    }
}

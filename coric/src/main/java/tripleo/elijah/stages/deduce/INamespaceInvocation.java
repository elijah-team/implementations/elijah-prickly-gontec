package tripleo.elijah.stages.deduce;

import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.NamespaceStatement;
import tripleo.elijah.stages.gen_fn.IGeneratedNamespace;

public interface INamespaceInvocation extends IInvocation {
    @NotNull DeferredObject<IGeneratedNamespace, Void, Void> resolveDeferred();

    @NotNull Promise<IGeneratedNamespace, Void, Void> resolvePromise();

    NamespaceStatement getNamespace();

    void setForFunctionInvocation(@NotNull IFunctionInvocation aFunctionInvocation);
}

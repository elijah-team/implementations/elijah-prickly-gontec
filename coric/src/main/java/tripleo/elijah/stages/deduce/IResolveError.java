package tripleo.elijah.stages.deduce;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.diagnostic.Diagnostic;
import tripleo.elijah.diagnostic.Locatable;
import tripleo.elijah.lang.LookupResult;

import java.io.PrintStream;
import java.util.List;

public interface IResolveError extends Diagnostic {
	@Override
	@NotNull String code();

	@Override
	@NotNull Severity severity();

	@Override
	@NotNull Locatable primary();

	@Override
	@NotNull List<Locatable> secondary();

	@Override
	void report(@NotNull PrintStream stream);

	@NotNull
	default String message() {
		if (resultsList().size() > 1)
			return "Can't choose between alternatives";
		else
			return "Can't resolve";
	}

	@NotNull List<LookupResult> resultsList();
}

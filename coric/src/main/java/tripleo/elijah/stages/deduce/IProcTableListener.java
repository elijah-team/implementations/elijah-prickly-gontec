package tripleo.elijah.stages.deduce;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.lang.*;
import tripleo.elijah.stages.gen_fn.*;
import tripleo.elijah.stages.instructions.IdentIA;
import tripleo.elijah.stages.instructions.InstructionArgument;
import tripleo.elijah.stages.instructions.IntegerIA;
import tripleo.elijah.stages.instructions.ProcIA;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;

public interface IProcTableListener extends  {

    void set_resolved_element_pte(@Nullable Constructable co,
                                  OS_Element e,
                                  IProcTableEntry pte,
                                  AbstractDependencyTracker depTracker);





    void finish(@Nullable Constructable co, @Nullable AbstractDependencyTracker depTracker, FunctionInvocation aFi, @Nullable GenType aGenType);


}

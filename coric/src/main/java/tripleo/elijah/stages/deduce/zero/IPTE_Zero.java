package tripleo.elijah.stages.deduce.zero;

import org.jdeferred2.Promise;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.ErrSink;
import tripleo.elijah.diagnostic.Diagnostic;
import tripleo.elijah.stages.gen_fn.IElementHolder;
import tripleo.elijah.stages.gen_fn.IGeneratedConstructor;
import tripleo.elijah.stages.gen_fn.IIdentTableEntry;

public interface IPTE_Zero {
    void foundCounstructorDef(@NotNull IGeneratedConstructor constructorDef,
                              @NotNull IIdentTableEntry ite,
                              @NotNull IDeduceTypes2 deduceTypes2,
                              @NotNull ErrSink errSink);

    Promise<IElementHolder, Diagnostic, Void> foundCounstructorPromise();

    void calculateConstructor(@NotNull IGeneratedConstructor constructorDef, @NotNull IIdentTableEntry ite, @NotNull IDeduceTypes2 deduceTypes2);
}

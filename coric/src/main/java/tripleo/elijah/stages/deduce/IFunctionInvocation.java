package tripleo.elijah.stages.deduce;

import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.lang.BaseFunctionDef;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.gen_fn.BaseGeneratedFunction;
import tripleo.elijah.stages.gen_fn.GeneratePhase;
import tripleo.elijah.stages.gen_fn.ITypeTableEntry;
import tripleo.elijah.stages.gen_fn.TypeTableEntry;
import tripleo.elijah.stages.gen_fn.WlGenerateFunction;

import java.util.List;

public interface IFunctionInvocation {
    void makeGenerated(@NotNull GeneratePhase generatePhase, @NotNull DeducePhase aPhase);

    @Nullable BaseGeneratedFunction getGenerated();

    void setGenerated(BaseGeneratedFunction aGeneratedFunction);

    BaseFunctionDef getFunction();

    ClassInvocation getClassInvocation();

    void setClassInvocation(@NotNull IClassInvocation aClassInvocation);

    INamespaceInvocation getNamespaceInvocation();

    void setNamespaceInvocation(INamespaceInvocation aNamespaceInvocation);

    @NotNull DeferredObject<BaseGeneratedFunction, Void, Void> generateDeferred();

    Promise<BaseGeneratedFunction, Void, Void> generatePromise();

    List<ITypeTableEntry> getArgs();

    boolean sameAs(@NotNull IFunctionInvocation aFunctionInvocation);

    WlGenerateFunction generateFunction(IDeduceTypes2 deduceTypes2, @NotNull OS_Element aElement);

    WlGenerateFunction generateFunction(@NotNull IDeduceTypes2 deduceTypes2, @NotNull OS_Module aModule);
}

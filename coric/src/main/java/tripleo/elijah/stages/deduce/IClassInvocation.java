package tripleo.elijah.stages.deduce;

import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.lang.TypeName;
import tripleo.elijah.stages.gen_fn.IGeneratedClass;

public interface IClassInvocation extends IInvocation {
    @NotNull DeferredObject<IGeneratedClass, Void, Void> resolveDeferred();

    void set(int aIndex, TypeName aTypeName, @NotNull OS_Type aType);

    @NotNull ClassStatement getKlass();

    @NotNull Promise<IGeneratedClass, Void, Void> resolvePromise();

    String getConstructorName();

    void setForFunctionInvocation(@NotNull IFunctionInvocation aFunctionInvocation);

    @NotNull Promise<IGeneratedClass, Void, Void> promise();
}

package tripleo.elijah;

import org.jetbrains.annotations.*;
import tripleo.elijah.comp.*;
import tripleo.elijah.lang.*;
import tripleo.elijah.util.*;

public class Out {
	private final ParserClosure pc;

	public Out(final String fn, final Compilation compilation, final boolean do_out) {
		pc = new ParserClosure(fn, compilation);
	}

	public static void println(final String s) {
		SimplePrintLoggerToRemoveSoon.println2(s);
	}

	public void FinishModule() {
		pc.module.finish();
	}

	public ParserClosure closure() {
		return pc;
	}

	public @NotNull OS_Module module() {
		return pc.module;
	}
}
